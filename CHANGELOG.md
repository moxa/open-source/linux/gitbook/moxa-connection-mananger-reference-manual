# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2025-01-17
### Added
   * support MCM doc v1.4 

## [1.1.1] - 2024-06-26
#### Fixed
   * add loss files

## [1.1.0] - 2024-06-24
#### Added
   * support MCM version 1.5

## [1.0.0]
### v1.0.0
   * support MCM version 1.4 
