/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Moxa Connection Manager Reference Manual',
  tagline: '',
  url: 'https://moxa.gitlab.io',
  baseUrl: '/open-source/linux/gitbook/moxa-connection-manager-reference-manual/', //DEBUG: baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/moxa_favicon.png',
  organizationName: 'Moxa', // Usually your GitHub org/user name.
  projectName: 'moxa-connection-manager-reference-manual', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Moxa Connection Manager Reference Manual',
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Moxa Connection Manager',
              to: '/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Moxa Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        pages: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
