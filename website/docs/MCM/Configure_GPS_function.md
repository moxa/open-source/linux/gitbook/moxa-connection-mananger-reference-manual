---
title: Configure GPS function
---

> :warning: Version 1.3.0 or above

Determine the default status of GPS : 

-   Enabled (default):

MCM will automatically enable the GPS function when system bootup. Users
can directly connect to the GPS port to obtain NMEA data.

-   Disabled:

MCM will disable the GPS function when system bootup.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531039/249531477.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531039/249531477.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531477"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231109-100415.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531039"
data-linked-resource-container-version="4" width="760" />


