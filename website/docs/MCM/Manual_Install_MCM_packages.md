---
title: Manual Install MCM packages
---

## Debian 12

#### Install ModemManager

apt install \[Package Directory\]/libmm-glib0\_.deb \[Package
Directory\]/ modemmanager\_.deb

#### Install NetworkManager

apt install \[Package Directory\]/libnm0\_.deb \[Package Directory\]/
network-manager\_.deb

#### Install MoxaConnectionManager

apt install \[Package Directory\]/libmcm0\_.deb \[Package Directory\]/
moxa-connection-manager\_.deb

## Ubuntu 22.04 HWE

#### Install ModemManager

apt install \[Package Directory\]/libmm-glib0\_.deb \[Package
Directory\]/ modemmanager\_.deb

#### Install NetworkManager

apt install \[Package Directory\]/libnm0\_.deb \[Package Directory\]/
network-manager\_.deb

#### Install MoxaConnectionManager

apt install \[Package Directory\]/libmcm0\_.deb \[Package Directory\]/
moxa-connection-manager\_.deb
