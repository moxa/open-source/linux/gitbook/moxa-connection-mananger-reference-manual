---
title: GUI
---

MoxaConnectionManager provides an easy-to-use GUI interface that allows
customers to configure their own profiles. Each use scenario will
introduce how to set it up through the GUI.

### Setting up MCM with GUI Configurator

``` java
mx-connect-mgmt configure
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530946/249531415.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530946/249531415.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531415"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-115426.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530946"
data-linked-resource-container-version="6" width="756" />

-   Configure Network Interfaces :

Configure the network interface and main functions.

-   Configure Log Level (Define syslog Log level):

    -   ERROR

    -   WARN

    -   INFO (Default)

    -   DEBUG

    -   TRACE

-   Set to Default MCM Configuration : 

Restores all MCM settings to default setting.

### **Configure Network Interfaces **

Set each interface type and function.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530946/249531500.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530946/249531500.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531500"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-024415.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530946"
data-linked-resource-container-version="6" width="760" />


