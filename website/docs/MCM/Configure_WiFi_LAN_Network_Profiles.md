---
title: Configure WiFi LAN Network Profiles
---

> :warning: only supported v1.6.0 or above

### Select Network Profile:

Configure profile 1, including SSID/Password related connection settings

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283106/306283124.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283106/306283124.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283124"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_11-37-25.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283106"
data-linked-resource-container-version="2" height="250" />

### Configure WiFi Setting

-   SSID

SSID name of WiFi AP

-   Password

WiFi AP password

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283106/306283130.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283106/306283130.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283130"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_11-38-52.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283106"
data-linked-resource-container-version="2" />

### Configure IP Method for WiFi

Only support IPv4 Method.

### **Configure IPv4 reloated setting**

-   IPv4 address:

IPv4 address of WIFI AP interface.

-   IPv4 Subnet address:

IPv4 netmask of WIFI AP interface.


