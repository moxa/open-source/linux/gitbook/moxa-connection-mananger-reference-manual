---
title: System Architecture
---

We develop the Moxa Connection Manager software functions primarily
using three open-source projects: wpa_supplicant, Network Manager, and
Modem Manager. Due to the varying hardware configurations of each Moxa
product, we utilize MoxaComputerInterface to abstract hardware-specific
settings and achieve platform independence. When MoxaComputerInterface
is not available, users can manually provide the necessary hardware
control scripts.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530922/249531503.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530922/249531503.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531503"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231107-110149.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530922"
data-linked-resource-container-version="5" width="742" />


