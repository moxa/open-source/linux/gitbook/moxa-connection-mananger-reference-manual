---
title: Configure Cellular Network Profiles
---

### Select Network Profile:

Configure each profile, including APN/SIM PIN/Check Alive related
connection settings

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531782.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531782.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531782"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231109-101927.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531015"
data-linked-resource-container-version="16" />

### Configure Modem Setting

-   APN :

SIM card APN. In version 1.4 and above, users can use an empty APN, and
MCM will automatically search for the corresponding APN based on the SIM
Card.

-   SIM Slot :

Select SIM slot.

-   PIN Code :

If the SIM card has PIN code protection enabled, the user needs to fill
in the PIN code. If not, there is no need to fill in a PIN.

-   Username / Password:

SIM card username/password。

-   RAT:

In version 1.4 and above, users can specify Cellular to use
AUTO/3G/4G/5G RAT for the cellular connection.

> :warning: Locking to 5G will cause connection issues under the 5G NSA
> architecture. Please ensure that the architecture is not NSA before
> locking to 5G.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531364.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531364.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531364"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20240418-032320.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531015"
data-linked-resource-container-version="16" width="760"
alt="image-20240418-032320.png" />

### Configure IP Method for Cellular

The default is IPv4, set the IP address method users want to connect to.
When set to IPv4v6, the interface will obtain IPv4 address and IPv6
address from the base station, otherwise when set to IPv4 or IPv6, only
IPv4 address or IPv6 address will be obtained.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531704.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531704.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531704"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024505.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531015"
data-linked-resource-container-version="16" width="760" />

### **Configure Connection Check-alive Method**

Select how to determine whether the connection is successful. Currently
two methods are supported: ping and check-ip-exist.

-   ping (default):

Use the ***ping*** command to check whether the connection between the
machine and the server is normal.

-   check-ip-exist

Check whether the machine has obtained the IPv4/IPv6 address. No
additional network traffic will be generated.

> :information_source: check-ip-exist method is only supported by v1.3
> and above.

-   ping-signalmonitor

Use the ***ping*** command to check whether the connection between the
machine and the server is normal. And check whether the Cellular signal
strength is greater than Threshold. If not, it means that the cellular
transmission speed and stability are poor.

-   check-ip-exist-signalmonitor

Check whether the machine has obtained the IPv4/IPv6 address. No
additional network traffic will be generated. And check whether the
Cellular signal strength is greater than Threshold. If not, it means
that the cellular transmission speed and stability are poor.

> :information_source: \*-signalmonitor method is only supported by v1.4
> and above.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531683.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531683.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531683"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231124-090258.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531015"
data-linked-resource-container-version="16" width="760" />

### **Configure Ping method settings**

-   IPv4 Ping Target Host :

The default value is 8.8.8.8. If you use IPv4/IPv4v6 to connect, use the
target host to check the connection. Domain names are also supported.

> :information_source: Domain names feature only supported by v1.3 and
> above.

-   IPv6 Ping Target Host:

If connecting using the IPv6 IP method, this target host will be used to
check the connection.

-   Ping Timeout:

The default is 3 seconds, indicating the timeout of the ping command.

-   Configure Check-alive Check Interval :

The default is 300 seconds, which means the ping method will be checked
for connectivity every 300 seconds.

-   Configure Check-alive Retry Threshold :

The default is 3, which means each check-alive will ping 3 times to
check for connectivity.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531553.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531553.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531553"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024541.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531015"
data-linked-resource-container-version="16" width="760" />

### **Time-control for interface connection by using ping method**

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531711"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-075450.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531074"
data-linked-resource-container-version="3" width="760" />

### **Configure Check-ip-exist method settings**

No need to configure other options.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531409.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531409.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531409"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024552.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531015"
data-linked-resource-container-version="16" width="760" />

### **Configure \*-signalmonitor method settings**

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531428.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531428.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531428"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20240418-030644.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531015"
data-linked-resource-container-version="16" width="760"
alt="image-20240418-030644.png" />

-   Configure Signal Monitor Long Interval

This value (in seconds) determines how frequently MCM checks if the
Cellular signal exceeds the threshold.

-   Configure Signal Monitor Threshold Type

When set to `SIGNAL-STRENGTH`, MCM will use the signal strength level
(`NONE/POOR/FAIR`..etc) as the threshold. Users can key
***mx-connect-mgmt nwk_info CellularX*** command to retrieve the current
signal strength level. When set to `RSRP or SINR`, the threshold will be
determined by the signal values (dB/dBm) of RSRP or SINR.

-   Configure Signal Monitor Threshold

Signal strength level or db/dbm.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531136/249531319.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531136/249531319.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531319"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20230908-024906.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531136"
data-linked-resource-container-version="1" width="846" />


