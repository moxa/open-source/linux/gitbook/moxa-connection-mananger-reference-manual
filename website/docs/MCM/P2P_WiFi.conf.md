---
title: P2P_WiFi.conf
---

### Moxa Connection Manager (MCM) -- Wi-Fi Direct (P2P) interface configure file

### Location

``` java
/etc/moxa/MoxaConnectionManager/interfaces/P2P_WiFi<ID>.conf
```

### Description

P2P_WiFi.conf is the configuration file of this WiFi Direct interface.
It contains an `Interface` section to control the profiles timeout, and
also contains the `Profile-1` sections. The`Profile-1` section is the
network settings for the WiFi Direct interface , including ip address
and SSID.

### File Format

The configuration file format is so-called key file (sort of ini-style
format). It consists of sections (groups) of key-value pairs. Lines
beginning with a '#' and blank lines are considered comments. Sections
are started by a header line containing the section enclosed in '\[' and
'\]', and ended implicitly by the start of the next section or the end
of the file. Each key-value pair must be contained in a section.

The main section structure is as follows:

``` java
[Interface] 
    ...
[Profile-1]
    ... 
```

### Interface Section

-   `activated=true|false`

Set this interface as active or inactive. When this interface is set as
`WAN`, `LAN`, or `Manual `type, setting it as inactive will cause MCM to
automatically ignore this interface, avoiding connections, keepalives,
failovers, and failbacks… etc.

-   `profile.wait-connected-timeout=<TIMEOUT>`

This value (in seconds) determines the maximum time MCM will try to
connect using a network profile before determining the connection is
unavailable.

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

### Profile-1 Section

> :warning: when interface is only use **Profile-1** as networking
> setting.

-   `ip-method=IPV4`

The interface is only support ipv4.

-   `ipv4-method=static`

    WiFi Direct only supports static IP configuration.

    -   `ipv4-address=<IPv4 Address>`

        This interface ipv4 address.

    -   `ipv4-netmask=<NETMASK>`

        This interface ipv4 netmask.

<!-- -->

-   `SSID=<SSID>|SerialNumber`

When the SSID is set to the `SerialNumber`, MCM will automatically
retrieve the machine's serial number and create a WiFi Direct interface
with the SSID as `DIRECT-<SerialNumber>`.When the SSID is set to
`<SSID>`, MCM will create a WiFi Direct interface with the SSID as
`DIRECT-<SSID>`.

-   `Password=<Password>`
