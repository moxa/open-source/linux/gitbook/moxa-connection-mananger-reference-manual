---
title: Reset to default
---

> :warning: only supported v1.3 or above

All MCM settings are restored to default values. There are two ways to
reset the configuration to default values:

-   Configure UI method:

Open the configuration UI `#mx-connect-mgmt configure` and select "Set
as default MCM settings".

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530921/249531307.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530921/249531307.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531307"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-085812.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530921"
data-linked-resource-container-version="5" width="760" />

-   MCM Cli command

Use the cli command to reset configuration files to default.

``` java
#mx-connect-mgmt default
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530921/249531715.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530921/249531715.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531715"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-085929.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530921"
data-linked-resource-container-version="5" width="760" />


