---
title: Setup WiFi-P2P Network Type
---

> :warning: only supported v1.4.0 or above

## Select WiFi-P2P interface

After selecting the wifi-p2p interface, the settings related to the
wifi-p2p connection will begin.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531106/249531418.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531106/249531418.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531418"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231123-102559.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531106"
data-linked-resource-container-version="5" width="760" />

## Network Type Setting

This page mainly sets information related to WiFi-P2P connection. The
default network type for WiFi-P2P is `None` and no connection behavior
will be performed on WiFi-P2P.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531106/249531289.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531106/249531289.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531289"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231123-102705.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531106"
data-linked-resource-container-version="5" width="760" />

Select WiFi-P2P network type as `None`/`LAN`type.

-   **LAN**

Defined as a local network type, no default route will be configured.
Because it is not a WAN type, it does not have the keep-alive function.
Profile-1 is used by default.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531106/249531790.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531106/249531790.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531790"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231123-103018.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531106"
data-linked-resource-container-version="5" width="760" />

**DHCP Server** : always enabled and cannot be set to disabled。

-   **None**

MCM does not control the WiFi-P2p interface


