---
title: Use Networkmanager to Control Cellular and WiFi on Debian 12 /Ubuntu 22.04
---

#### Establish a connection to a WiFi Access Point using the Wifi 6 module

``` java
nmcli connection add type wifi con-name <CONNECTION NAME> ifname <Device name> ssid <AP's SSID>
nmcli c modify <CONNECTION NAME> wifi-sec.key-mgmt wpa-psk wifi-sec.psk <AP's Password>

<Device name>: e.g wlp17s0 or wlp18s0
e.g
nmcli connection add type wifi con-name wifi6 ifname wlp18s0 ssid "TP-Link"
nmcli c modify wifi6 wifi-sec.key-mgmt wpa-psk wifi-sec.psk <password>
```

#### Configure the WiFi module as a WiFi access point

``` java
nmcli con add type wifi ifname <Device name> con-name <Hostspot Name> autoconnect yes ssid <SSID>
nmcli con modify <Hostspot Name> 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared
nmcli con modify <Hostspot Name> wifi-sec.key-mgmt wpa-psk
nmcli con modify <Hostspot Name> wifi-sec.psk <Password>
nmcli con up <Hostspot Name>

<Device name>: e.g wlp17s0 or wlp18s0
e.g
nmcli con add type wifi ifname wlp17s0 con-name WiFi5-Hostspot autoconnect yes ssid Hostspot
nmcli con modify WiFi5-Hostspot 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared
nmcli con modify WiFi5-Hostspot wifi-sec.key-mgmt wpa-psk
nmcli con modify WiFi5-Hostspot wifi-sec.psk "moxa12345"
nmcli con up WiFi5-Hostspot
```

#### Establish a 4G/5G connection

``` java
nmcli c add type gsm ifname cdc-wdm0 con-name <CONNECTION NAME>  apn <APN>

e.g 
nmcli c add type gsm ifname cdc-wdm0 con-name moxa-lte-con apn internet
```

### Reference

<a
href="https://gist.github.com/narate/d3f001c97e1c981a59f94cd76f041140"
data-card-appearance="inline"
rel="nofollow">https://gist.github.com/narate/d3f001c97e1c981a59f94cd76f041140</a>

<a
href="https://ubuntu.com/core/docs/networkmanager/configure-wifi-connections"
data-card-appearance="inline"
rel="nofollow">https://ubuntu.com/core/docs/networkmanager/configure-wifi-connections</a>

<a
href="https://techship.com/faq/using-network-manager-and-modem-manager-in-linux-to-automatically-establish-a-connection-and-configure-ip-details/"
data-card-appearance="inline"
rel="nofollow">https://techship.com/faq/using-network-manager-and-modem-manager-in-linux-to-automatically-establish-a-connection-and-configure-ip-details/</a>
