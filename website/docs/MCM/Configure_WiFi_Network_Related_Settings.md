---
title: Configure WiFi Network Related Settings
---

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531499.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531499.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531499"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-100552.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531089"
data-linked-resource-container-version="2" width="760" />

### Configure Network Profile Priority

The default value is \[Profile-1, Profile-2\]. This setting is a profile
priority list for WAN type connections. Profile priority list is the
order used for reconnecting (separated by commas).

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531511.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531511.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531511"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231207-070857.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531089"
data-linked-resource-container-version="2" width="760" />

### Configure Network Profile Retry Threshold

The default is to retry 2 times, which means that the same Profile retry
connection process will be repeated twice before switching to the next
Profile.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531630.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531630.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531630"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231109-102700.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531089"
data-linked-resource-container-version="2" width="760" />

### Configure Network Profile Timeout

The setting is the maximum waiting time for each Profile to determine
whether the Profile cannot be connected, The default value is 90
seconds.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531349.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531349.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531349"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231207-070938.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531089"
data-linked-resource-container-version="2" width="760" />

### Profiles reconnection process

Example using default settings (profile priority=1,2, profile retry
threshold=2, profile timeout=90)

Profile1 will retry 2 times. If the interface cannot connect, profile2
will be used to reconnect. The meaning of 90 seconds is the waiting time
for each Pofile. When the waiting time is up, if there is still no
connected, the interface will be judged as 'unable to connect'.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531663.png" class="image-left"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531089/249531663.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531663"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231109-104743.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531089"
data-linked-resource-container-version="2" width="168" />

### Configure Network Profile setting

[Configure WiFi Network Profiles](Configure_WiFi_WAN_Network_Profiles)


