---
title: Configuration file structure
---

-   Configuration Location:

    -   /etc/moxa/MoxaConnectionManager/

        -   MoxaConnectionManager.conf (Configure network interfaces for
            MCM management.)

        -   interfaces (Network interface profile setting)

            -   Cellular***X***.conf 

            -   LAN***X***.conf (Ex. LAN1，LAN2)

            -   WiFi***X***.conf 

-   The file name is fixed and cannot be changed.

-   The interface name can be obtained through the cli command.

``` c
mx-connect-mgmt ls
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531041/249531355.png" class="image-left"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531041/249531355.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531355"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231108-080219.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531041"
data-linked-resource-container-version="7" width="656" />


