---
title: Seamless Failover
---

> :warning: Only supports v1.3 or above

-   Disabled: (default)

When the primary interface connection becomes unavailable, MCM will
attempt to establish a connection using all the pre-configured profiles
before switching to the backup interface. There will be a period of
downtime during the failback process.

-   Enabled:

When the primary interface becomes unavailable, MCM will seamlessly
switch the default gateway to the already connected backup interface,
avoiding any downtime during the failback process. However, it is
essential to be aware using ping as the keep-alive method for the backup
interface might incur some data costs.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531157/249531345.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531157/249531345.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531345"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-083729.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531157"
data-linked-resource-container-version="9" width="760" />

#### a) Seamless Failover (without Profile informaction)

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531123/249531717.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531123/249531717.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531717"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-082020.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531123"
data-linked-resource-container-version="2" width="760" />

#### a) Seamless Failover(with Profile informaction)

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531196/249531642.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531196/249531642.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531642"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-081703.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531196"
data-linked-resource-container-version="1" width="760" />


