---
title: Control Moxa-Connection-Manager
---

``` c
#include <libmcm/mcm-control.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(void) {
    printf("USAGE: control start/stop/reload/state \n");
}

int control_mcm(char *command) {
    if (!strcmp(command, "start"))
        mcm_control_start();
    else if (!strcmp(command, "stop"))
        mcm_control_stop();
    else if (!strcmp(command, "reload"))
        mcm_control_reload();
    else if (!strcmp(command, "state"))
        printf("Current State of Moxa Connection Manager : %s\n", mcm_control_get_state());
    else
        usage();
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        usage();
        return -1;
    }
    return control_mcm(argv[1]);
}
```
