---
title: Using 'networking' service to control Ethernet instend of MCM
---

### Remove ‘networking.service’ conflicts field

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531214/249531768.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531214/249531768.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531768"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20240515-095829.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531214"
data-linked-resource-container-version="1" width="1800"
alt="image-20240515-095829.png" />

### Setting ‘managed’ is ‘false’ on NetworkManager.conf

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531214/249531552.png" class="image-left"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531214/249531552.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531552"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20240515-100138.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531214"
data-linked-resource-container-version="1" width="864"
alt="image-20240515-100138.png" />

### Reboot System


