---
title: User Manual ChangeLog
---

<table class="wrapped confluenceTable">
<tbody>
<tr>
<th class="highlight-#deebff confluenceTh"
data-highlight-colour="#deebff"><p><strong>Version</strong></p></th>
<th class="highlight-#deebff confluenceTh"
data-highlight-colour="#deebff"><p><strong>Description of
Change</strong></p></th>
<th class="highlight-#deebff confluenceTh"
data-highlight-colour="#deebff"><p><strong>Reason</strong></p></th>
<th class="highlight-#deebff confluenceTh"
data-highlight-colour="#deebff"><p><strong>Date</strong></p></th>
<th class="highlight-#deebff confluenceTh"
data-highlight-colour="#deebff"><p><strong>Author</strong></p></th>
</tr>
&#10;<tr>
<td class="highlight-#fffae6 confluenceTd"
data-highlight-colour="#fffae6"><p>1.0</p></td>
<td class="confluenceTd"><p>Init release (support MCM v1.4)</p></td>
<td class="confluenceTd"><p>Init Release</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>11 Dec 2023</p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Tzongyen Lin (林宗彥)</p>
</div></td>
</tr>
<tr>
<td class="highlight-#fffae6 confluenceTd"
data-highlight-colour="#fffae6"><p>1.1</p></td>
<td class="confluenceTd"><ul>
<li><p>rewrite by chatGPT</p></li>
<li><p>Add ‘signal-monitor’ description</p></li>
<li><p>Add ‘check-ip-exist’ configuration description</p></li>
<li><p>Add ‘Fixed RAT’ description</p></li>
<li><p>Add ‘AutoAPN’ description</p></li>
</ul></td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>12 Apr 2024</p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Tzongyen Lin (林宗彥)</p>
</div></td>
</tr>
<tr>
<td class="highlight-#fffae6 confluenceTd"
data-highlight-colour="#fffae6"><p>1.2</p></td>
<td class="confluenceTd"><ul>
<li><p>Add ‘modules.initialize.exec’ description</p></li>
<li><p>Add ‘modules.finalize.exec’ description</p></li>
<li><p>Add switch sim command description</p></li>
<li><p>Add module.reset.exec description</p></li>
</ul></td>
<td class="confluenceTd"><ul>
<li><p>sync form MCM v1.5</p></li>
</ul></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>14 Jun 2024</p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Tzongyen Lin (林宗彥)</p>
</div></td>
</tr>
<tr>
<td class="highlight-#fffae6 confluenceTd"
data-highlight-colour="#fffae6"><p>1.3 </p></td>
<td class="confluenceTd"><ul>
<li>Add 'gps.antenna.type' option description</li>
<li>Add 'auto-gpsd-sync' option description</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>Add loss description </li>
</ul></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>02 Oct 2024 </p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Tzongyen Lin (林宗彥) </p>
</div></td>
</tr>
<tr>
<td class="highlight-#fffae6 confluenceTd"
data-highlight-colour="#fffae6"><p>1.4</p></td>
<td class="confluenceTd"><ul>
<li>Add 'Multi-WAN' releated description</li>
<li>Add WiFi AP releated description</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>sync form MCM v1.6</li>
</ul></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>15 Nov 2024 </p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Tzongyen Lin (林宗彥) </p>
</div></td>
</tr>
</tbody>
</table>

  
