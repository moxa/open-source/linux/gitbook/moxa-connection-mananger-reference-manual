---
title: Cellular D-bus Properties
---

|  |  |
|----|----|
| **Cellular Interface** | **Description** |
| common |  |
| "Name" | The name of the interface. |
| "DeviceType" | The interface type: Ethernet/WiFi/Modem |
| "DeviceName" | The device name of the interface. |
| "Enabled" | If the interface is managed. |
| "Network" | The interface works as WAN or LAN. |
| "WANPriority" | The interface's link priority when works as WAN. |
| "DefaultRoute" | If the interface is current default route. |
| "Profiles" | The list of profile names of the interface. (e.g Profile1, Profile2) |
| "ProfileName" | The current profile Name of the interface. ( \>= Version 1.3) |
| "MACAddress" | The MAC address of the interface. |
| "IPV4AddressMethod" | The method to get IPV4 address for the interface. (static or DHCP) |
| "IPV4Address" | The IPV4 address of the interface. |
| "IPV4Netmask" | The IPV4 netmask of the interface. |
| "IPV4Gateway" | The IPV4 gateway of the interface. |
| "IPV4PrimaryDNS" | The primary DNS server of the interface. |
| "IPV4SecondaryDNS" | The secondary DNS server of the interface. |
| "IPV6AddressMethod" | The method to get IPV6 address for the interface. ( only support ‘auto’) |
| "IPV6Address" | The IPV6 address of the interface. |
| "IPV6Netmask" | The IPV6 netmask of the interface. |
| "IPV6Gateway" | The IPV4 gateway of the interface. |
| "IPV6PrimaryDNS" | The primary DNS server of the interface. |
| "IPV6SecondaryDNS" | The secondary DNS server of the interface. |
| "ConnectionState" | The connection checking status of the interface. |
| "ErrorReason" | Error log when executing the interface ( \>= Version 1.3) |
| cellular special |  |
| "TACLAC" | TAC/LAC number |
| “Operator" | Carrier Name |
| "SimSlot" | Currently used SIM card slot. |
| "DeviceImei" | Cellular Module IMEI number. |
| “SimImsi" | SIM IMSI number. |
| "SimIccid" | SIM ICCID number. |
| "NetworkRat" | Network RAT ( 3G /4G/5G/5G(NSA) ) |
| "SignalStrength" | Signal Stregth (0 ~ 4 level) |
| "CellID" | Cell ID |
| "PINRetries" | Remaining pin retries for SIM card. |
| "UmtsRssi" | 3G RSSI signal strength |
| "UmtsEcio" | 3G ECIO signal strength |
| "LteRsrp" | 4G RSRP signal strength |
| "LteRssnr" | 4G RSSNR signal strength |
| "ModemState" | Modme Current State.(e.g. Power off , SIM Missing Connectd) |
| "ModemName" | Cellular Module Name |
| "ModemVersion" | Cellular Module firmware version |
| "SWResetCount" | Cellular Module SW reset count ( \>= Version 1.3) |
| "HWResetCount" | Cellular Module HW reset count ( \>= Version 1.3) |
| "TotalSimSlots" | Total number of SIM slots on the machine ( \>= Version 1.3) |
| “Alarm” | Modem alarm notify. ( \>= Version 1.3) |
