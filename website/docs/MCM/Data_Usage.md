---
title: Data Usage
---

Check the data usage of a specified interface between a specified start
and end date.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530956/249531799.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530956/249531799.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531799"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-065853.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530956"
data-linked-resource-container-version="2" width="760" />

### Check Data usage

``` java
#mx-connect-mgmt datausage -s <Start_Date> -t <End_Date> <Interface>
```

Example: Check the data usage for Cellular1

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530956/249531632.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530956/249531632.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531632"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-070026.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530956"
data-linked-resource-container-version="2" width="760" />

### Reset Database

``` java
#mx-connect-mgmt datausage -r <Interface>
```

Example: Reset Database for WiFi

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530956/249531490.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530956/249531490.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531490"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-070131.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530956"
data-linked-resource-container-version="2" width="760" />


