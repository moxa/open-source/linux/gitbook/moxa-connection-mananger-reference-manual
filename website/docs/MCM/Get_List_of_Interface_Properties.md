---
title: Get List of Interface Properties
---

``` c
#include <libmcm/mcm-eth-info.h>
#include <libmcm/mcm-interface-info.h>
#include <libmcm/mcm-modem-info.h>
#include <libmcm/mcm-network-info.h>
#include <libmcm/mcm-wifi-info.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(void) {
    printf("USAGE: list_property [Interface Name] or list_property [Interface Name] [Property Nmae]\n");
}

int show_all_property(char *interface_name) {
    int ret = 0;
    int num = 0;

    mcm_network_info   network_info;
    mcm_property_info *info_arr;

    memset(&network_info, 0, sizeof(mcm_network_info));

    ret = mcm_get_network_info(interface_name, &network_info);
    if (ret != 0) {
        printf("ERROR: Get interface (%s) information failed:%d\n", interface_name, ret);
        return -1;
    }

    if (!strncmp(network_info.device_type, "Ethernet", strlen("Ethernet")))
        ret = mcm_ethernet_get_all_properties(interface_name, &num, &info_arr);
    else if (!strncmp(network_info.device_type, "WiFi", strlen("WiFi")))
        ret = mcm_wifi_get_all_properties(interface_name, &num, &info_arr);
    else if (!strncmp(network_info.device_type, "Modem", strlen("Modem")))
        ret = mcm_modem_get_all_properties(interface_name, &num, &info_arr);

    if (ret != 0) {
        printf("ERROR: Get interface (%s) property list failed:%d\n", interface_name, ret);
        return -1;
    }

    printf("Property List:\n");
    for (int i = 0; i < num; i++) {
        printf("\t%s = %s\n", info_arr[i].propertyName, info_arr[i].propertyValue);
    }
    mcm_free_property_info(num, info_arr);
    return 0;
}

int get_property(char *interface_name, char *property_name) {
    int   ret = 0;
    char *device_type;
    char *property_val;

    ret = mcm_network_get_property(interface_name, "DeviceType", &device_type);
    if (ret != 0) {
        printf("ERROR: Get interface (%s) DeviceType failed:(%d)%s\n", interface_name, ret, mcm_get_error_message(ret));
        return ret;
    }

    ret = mcm_network_get_property(interface_name, property_name, &property_val);
    if (ret == 0) {
        printf("%s:%s\n", property_name, property_val);
        return 0;
    }

    if (!strncmp(device_type, "Ethernet", strlen("Ethernet")))
        ret = mcm_ethernet_get_property(interface_name, property_name, &property_val, false);
    else if (!strncmp(device_type, "WiFi", strlen("WiFi")))
        ret = mcm_wifi_get_property(interface_name, property_name, &property_val, false);
    else if (!strncmp(device_type, "Modem", strlen("Modem")))
        ret = mcm_modem_get_property(interface_name, property_name, &property_val, false);

    if (ret != 0) {
        printf("ERROR: Get interface (%s) property (%s) failed:%d\n", interface_name, property_name, ret);
        return -1;
    }

    printf("%s:%s\n", property_name, property_val);
    return 0;
}

int main(int argc, char *argv[]) {

    if (argc == 2)
        return show_all_property(argv[1]);
    else if (argc == 3)
        return get_property(argv[1], argv[2]);
    else {
        usage();
        return -1;
    }
}
```

------------------------------------------------------------------------
