---
title: Cellular Disagnosis
---

Before mass deployment, users can use the CLI command to confirm whether
the connected profile and environment are normal. MCM also will execute
a set of AT commands to get the status of the module in order to further
analyze. Users can send logs to Moxa to speed up cellular connection
failure analysis

### Using MCM Cli Commands to perform diagnosis on the cellular network

``` java
#mx-connect-mgmt debug diag <Cellualr_Interface>
```

Diag command can be executed directly without stopping MCM first.
Determine which Profiles to use for Diagnostic Analysis based on the
value of "Configure Network Profile Priority".

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530898/249531633.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530898/249531633.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531633"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-071750.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530898"
data-linked-resource-container-version="2" width="757" />

### Diagnosis and Analysis Content

Use the settings of Cellular Profile-X to diagnose the following
conditions:

-   Primary and secondary antenna signal strength checking. (Diagnose
    whether there is a missing antenna or weak signal strength)

-   SIM status checking.

    -   SIM OK

    -   SIM PIN

    -   SIM PUK

    -   SIM Missing

-   SIM PIN Verify.

-   APN

    -   Modem connected

-   Received Signal Strength

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530898/249531706.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530898/249531706.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531706"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-071146.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530898"
data-linked-resource-container-version="2" width="760" />

### After diagnosis

Users need to restart mcm to restore the connection.

``` java
#mx-connect-mgmt stop
#mx-connect-mgmt start
```

> **Limitation** : The Diagnosis Analysis function of MCM V1.3 version
> will definitely use the `Ping `Target method for diagnosis, even if
> `check-ip-exist method` is set in User Profile-x. MCM v1.4 fixes this
> limitation


