---
title: List All Interfaces
---

``` c
#include <libmcm/mcm-interface-info.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFERSIZE 128

int main(int argc, char *argv[]) {
    mcm_interface_info *interface_list;
    int                 num = 0;
    int                 ret = 0;
    char                buffer[BUFFERSIZE];
    mcm_network_info    network_info;

    memset(buffer, 0, sizeof(buffer));
    memset(&network_info, 0, sizeof(mcm_network_info));

    ret = mcm_interface_get_all_list(&num, &interface_list);
    if (ret != 0) {
        printf("ERROR: Get interface list failed:%d\n", ret);
        return ret;
    }

    printf("Total Interface Number : %d\n", num);
    for (int i = 0; i < num; i++) {
        printf("\%s\n", interface_list[i].interface_name);
        printf("\tDevice Name:%s\n", interface_list[i].device_name);
        printf("\tDevice Type:%s\n", interface_list[i].device_type);
    }
    return ret;
}
```
