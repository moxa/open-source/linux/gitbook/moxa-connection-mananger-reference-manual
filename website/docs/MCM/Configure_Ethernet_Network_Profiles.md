---
title: Configure Ethernet Network Profiles
---

### Select Network Profile:

Configure each profile, including SSID/Password/Check Alive related
connection settings

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531592.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531592.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531592"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-111931.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="760" />

### Configure IP Method for Ethernet

The default is IPv4, set the IP address method users want to connect to.
When set to IPv4v6, the interface will obtain IPv4 address and IPv6
address from the base station, otherwise when set to IPv4 or IPv6, only
IPv4 address or IPv6 address will be obtained.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531460.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531460.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531460"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024505.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="762" />

#### Configure IPv4 Method

IPv4-method supports three methods: `static`, `link-local`, and `dhcp`.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531326.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531326.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531326"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-112857.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="762" /><img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531371.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531371.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531371"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-112835.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="760" />

-   Static:

Set static IPv4 information: `IPv4 Address`, `Netmask` ,
`Default Gateway` , `DNS. `

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531548.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531548.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531548"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-113246.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="633" />

-   Link-local:

Link local means "only used for communication within the same broadcast
domain." For IPv4, the IP address range of 169.254.0.0/16 will be
assigened.

-   dhcp:

Dynamically obtain ipv4 address/dns/gateway from dhcp server.

#### Configure IPv6 Method

Currently only support Stateless Address Auto-Configuration (SLACC) and
Stateless for DHCPv6。

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531546.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531546.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531546"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-114211.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="633" />

### **Configure Connection Check-alive Method**

Select how to determine whether the connection is successful. Currently
two methods are supported: ping and check-ip-exist.

-   ping (default):

Use the ***ping*** command to check whether the connection between the
machine and the server is normal.

-   check-ip-exist

Check whether the machine has obtained the IPv4/IPv6 address. No
additional network traffic will be generated.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531362.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531362.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531362"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024514.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="760" />

### **Configure Ping method settings**

-   IPv4 Ping Target Host :

The default value is 8.8.8.8. If you use IPv4/IPv4v6 to connect, use the
target host to check the connection. Domain names are also supported.

> :information_source: Domain names feature only supported by v1.3 and
> above.

-   IPv6 Ping Target Host:

If connecting using the IPv6 IP method, this target host will be used to
check the connection.

-   Ping Timeout:

The default is 3 seconds, indicating the timeout of the ping command.

-   Configure Check-alive Check Interval :

The default is 300 seconds, which means the ping method will be checked
for connectivity every 300 seconds.

-   Configure Check-alive Retry Threshold :

The default is 3, which means each check-alive will ping 3 times to
check for connectivity.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531838.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531838.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531838"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024541.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="760" />

### **Time-control for interface connection by using ping method**

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531711"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-075450.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531074"
data-linked-resource-container-version="3" width="760" />

### **Configure Check-ip-exist method settings**

No need to configure other options.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531330.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531036/249531330.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531330"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-114342.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531036"
data-linked-resource-container-version="5" width="762" />


