---
title: Network Interface Status
---

### Checking the Interface and Connection Status

Use the following command to check the interface and connection status.

``` java
#mx-connect-mgmt nwk_info <Interface>
#mx-connect-mgmt nwk_info -a <Interface>
#mx-connect-mgmt nwk_info -m <Interface>
#mx-connect-mgmt nwk_info -c <Interface>
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531325.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531325.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531325"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-092105.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530978"
data-linked-resource-container-version="1" width="760" />

### Cellular informaction:

-   WAN Priority : ( start from 1, 0 indicates non-wan type)

Priority interface list for WAN.

-   Network Type :

Define for WAN or Manual Type。

-   Modem State:

Detailed modem status such as SIM PIN locked/No SIM/Connected to base
station...etc.。

-   Connection Status:

Current connection status

-   Radio Access Tech :

    -   LTE

    -   UMTS (3G)

    -   5G

    -   5G(NSA)

-   Signal Strength : The order of strength from weak to strong is:

    -   None/Very Poor

    -   Poor

    -   Fair

    -   Good

    -   Excellent

-   Operator Name :

Carrier.

-   Unlock Retries :

    -   Ramain PIN retry count.

-   SIM Slot :

    -   Currently used SIM slot

-   IMSI / ICCID :

SIM info.

-   TAC /IMEI:

Module info

-   Modem Version

Module firmware version

-   Modem Name:

Module model name

-   Default Route :

Users can use this field to know whether the interface is currently the
default routing interface.

-   IPv4 and IPv6 Address

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531783.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531783.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531783"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-092238.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530978"
data-linked-resource-container-version="1" width="760" />

### WiFi informaction

-   WAN Priority : ( start from 1, 0 indicates non-wan type)

Priority interface list for WAN.

-   Network Type :

Define for WAN or Manual Type。

-   SSID:

SSID name of the connected WiFi AP

-   Secure mode :

Authentication security mode for the connected WiFi AP.

-   Connection Status :

Current connection status

-   Frequenct:

The frequency of the connected WiFi AP.

-   Signal :

Signal strength. WiFi is shown as %.

-   Default Route :

Users can use this field to know whether the interface is currently the
default routing interface.

-   IPv4 and IPv6 address。

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531609.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531609.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531609"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-093409.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530978"
data-linked-resource-container-version="1" width="751" />

### Ethernet informaction

-   WAN Priority : ( start from 1, 0 indicates non-wan type)

Priority interface list for WAN.

-   Network Type :

Define for WAN, LAN or Manual Type。

-   Lin Detected : true or false

-   Connection Status :

Current connection status

-   Default Route :

Users can use this field to know whether the interface is currently the
default routing interface. LAN type is false

-   IPv4 and IPv6 address。

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531481.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530978/249531481.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531481"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-094021.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530978"
data-linked-resource-container-version="1" width="760" />


