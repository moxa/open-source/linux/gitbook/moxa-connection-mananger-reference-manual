---
title: Simple configuration for Cellular connection
---

## Edit the interfaces

|                              |
|:-----------------------------|
| `#mx-connect-mgmt configure` |

## Setup Cellular as default gateway

-   Select Cellular interface

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283208.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283208.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283208"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-21-45.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283209.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283209.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283209"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-23-17.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

## Setup Cellular Profiles

-   Setup to use Profile1

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283211.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283211.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283211"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-25-16.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283202.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283202.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283202"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-14-50.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283205.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283205.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283205"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-15-37.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

-   Configure the settings related to Profile 1

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/249531364.png" draggable="false"
data-image-src="https://wiki.moxa.com/download//open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531015/image-20240418-032320.png?version=1&amp;modificationDate=1713410225000&amp;api=v2"
width="720" alt="image-20240418-032320.png" />

-   -   APN : SIM card APN. In version 1.4 and above, users can use an
        empty APN, and MCM will automatically search for the
        corresponding APN based on the SIM Card.

<!-- -->

-   -   SIM Slot : Select SIM slot.

<!-- -->

-   -   PIN Code : If the SIM card has PIN code protection enabled, the
        user needs to fill in the PIN code. If not, there is no need to
        fill in a PIN.

<!-- -->

-   -   Username / Password: SIM card username/password

  

-   Exit /Save and reload

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283219.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283219.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283219"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-27-18.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283220.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283220.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283220"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-27-39.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283222.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283222.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283222"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_12-28-0.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="720" />

## Check the cellular connection status.

|                                       |
|:--------------------------------------|
| `#mx-connect-mgmt nwk_info Cellular1` |

  

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283242.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/306283184/306283242.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283242"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_13-4-25.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="306283184"
data-linked-resource-container-version="6" width="719" />


