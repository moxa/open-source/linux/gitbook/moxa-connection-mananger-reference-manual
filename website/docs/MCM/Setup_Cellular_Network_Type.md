---
title: Setup Cellular Network Type
---

## Select cellular interface

After selecting the cellular interface, the settings related to the
cellular connection will begin.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531138/306283150.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531138/306283150.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283150"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_11-45-42.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531138"
data-linked-resource-container-version="7" height="214" /> 

## Network Type Setting

This page mainly sets information related to Cellular connection. The
default network type for Cellular is `None `and no connection behavior
will be performed on Cellular.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531138/306283151.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531138/306283151.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283151"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_11-46-36.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531138"
data-linked-resource-container-version="7" width="693" />

Select Cellular network type as `None`/`WAN`/`Manual/Multi-WAN `type.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531138/306283153.png" draggable="false"
data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531138/306283153.png"
data-unresolved-comment-count="0" data-linked-resource-id="306283153"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-2024-11-18_11-47-18.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531138"
data-linked-resource-container-version="7" width="691" />

-   **WAN**:

After being configured as a WAN. The Cellular interface will be added to
the ***failover priority list***, that is, it will have a keep-alive
function. The MCM will keep Cellular active and configure the default
gateway according to the priority list. Please reference the
Failover/Failure chapter.

-   **Manual**

MCM only helps establish connections, but it does not have keepalive and
failover/failback capabilities. This is a one-time connection. By
default, only Profile-1 can be used.

MCM will not set these interfaces as the default gateway.

-   **Mulit-WAN**

When set to Multi-WAN, this interface will be NOT added to the default
gateway list but allow MCM to apply automatic keep-alive. Users can
access and control the machine through this interfaces without affecting
default WAN interfaces. (v1.6 or above)

-   **None**

MCM does not control Cellular interface


