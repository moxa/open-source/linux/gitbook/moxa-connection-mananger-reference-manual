---
title: LAN.conf
---

### Moxa Connection Manager (MCM) ethernet interface configure file

### Location

``` java
/etc/moxa/MoxaConnectionManager/interfaces/LAN<ID>.conf
```

### Description

LAN.conf is the configuration file of this Ethernet interface. It
contains an `Interface` section to control the priority list of profiles
and timeout, and also contains the different `Profile` sections. Each
`Profile` section is the network settings for the Ethernet interface ,
including ip usage method and how to judge that the interface is
connected.

### File Format

The configuration file format is so-called key file (sort of ini-style
format). It consists of sections (groups) of key-value pairs. Lines
beginning with a '#' and blank lines are considered comments. Sections
are started by a header line containing the section enclosed in '\[' and
'\]', and ended implicitly by the start of the next section or the end
of the file. Each key-value pair must be contained in a section.

The main section structure is as follows:

``` java
[Interface] 
    ...
[Profile-1]
    ...
[Profile-2]
    ...
[Profile-x]    
```

### Interface Section

-   `activated=true|false`

Set this interface as active or inactive. When this interface is set as
`WAN`, `LAN`, or `Manual `type, setting it as inactive will cause MCM to
automatically ignore this interface, avoiding connections, keepalives,
failovers, and failbacks… etc.

-   `connection.always-keep-alive=true|false`

You can enable this setting if a seamless failover experience is
required, meaning if a backup interface is set to always keep-alive,
then MCM can failover to a ready-to-use backup connection without the
initialization downtime.

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   `profile.retry-threshold=<TIMEOUT>`

This value (in counts) determines the maximum attempts MCM will try to
connect using a network profile before failover to the next profile in
the priority list

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   `profile.wait-connected-timeout=<TIMEOUT>`

This value (in seconds) determines the maximum time MCM will try to
connect using a network profile before determining the connection is
unavailable.

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   `profile.failover-priority=<PROFILE ID1>,<PROFILE ID2>`

The network profile numbers in order of priority. For example, enter 1,3
means MCM will use Profile-1 to connect and failover to Profile-3 when
Profile-1 cannot connect or become unavailable.

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   `dhcp-server.enabled=true|false`

    -   `dhcp-server.ip-lease-range=<Start IP Address>, <End IP Address>`

        The range must belong to the subnet of the bridge interface. For
        example, when `ipv4.address` filed is set to 192.168.0.127 and
        `ipv4-netmask` is set to 255.255.255.0, then the dhcp ip range
        needs to be a subset of 192.168.0.1 to 192.168.0.254. or when
        `ipv4.address` filed is set to 192.168.0.127 and `ipv4-netmask`
        is set to 255.255.0.0, then the dhcp ip range needs to be a
        subset of 192.168.0.1 to 192.168.254.254.

    -   `dhcp-server.ip-lease-time=<TIME>`

        If the lease time is given, then leases will be given for that
        length of time. The lease time is in seconds, or minutes (eg
        45m) or hours (eg 1h) or days (2d) or weeks (1w) or "infinite".
        If not given, the default lease time is one hour for IPv4. The
        minimum lease time is two minutes.

#### Profile-\[ID\] Section

> :warning: LAN/Manual type will only use **Profile-1** setting

-   `ip-method=IPV4|IPV6|IPV4V6`

When set to `IPV4V6`, the interface will obtain ipv4 address and ipv6
address from the base station, otherwise when set to `IPV4` or `IPV6`,
only ipv4 address or ipv6 address will be obtained.

-   `ipv4-method=static|link-local|dhcp`

    When `ipv4-method` is set to `link-local`, the `ipv4-address` is
    randomly generated from the local machine.

    When `ipv4-method` is set to `dhcp`, `ipv4-address` `ipv4-netmask`
    ... etc. are obtained from the dhcp server.

    when `ipv4-method` is set to `static`, the other settings are as
    follows.

    -   `ipv4-address=<IPv4 Address>`

        This interface ipv4 address.

    -   `ipv4-netmask=<NETMASK>`

        This interface ipv4 netmask.

    -   `ipv4-gateway=<GATEWAY>`

        This interface ipv4 gateway.

    -   `primary-dns=<DNS Server>`

        This interface primary dns address.

    -   `secondary-dns=<DNS Server>`

        this interface secondary dns address.

-   `ipv6-method=auto`

Currently only support Stateless DHCPv6.

-   `connection.check-method=ping | check-ip-exist`

    When the field is set to `check-ip-exist`, MCM will only detect if
    the ethernet cable is removed.

    When the field is set to `ping`that use the `ping` command to check
    if the interface network is connected.

-   `ping.ipv4-target-host=<SERVER IPv4 Address>`

The ipv4 address of the target host

-   `ping.ipv6-target-host=<SERVER IPv6 Address>`

The ipv6 address of the target host

-   `ping.timeout=<TIMEOUT>`

This value (in seconds) determines the maximum time MCM will try to ping
the configured target host and wait for a response before determining
the connection is unavailable.

-   `connection.check-alive-interval=<INTERVAL>`

This value (in seconds) determines how often MCM check the connection is
alive.

-   `connection.check-failed-threshold=<THRESHOLD>`

This value determines the maximum attempts MCM will check the connection
status before concluding the connection is not available
