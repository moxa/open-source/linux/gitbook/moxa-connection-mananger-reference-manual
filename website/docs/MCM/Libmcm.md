---
title: Libmcm
---

libmcm is the client library of MoxaConnectionManager (a service
developed by moxa for managing network interfaces).
MoxaConnectionManager aims to establish a unified layer to handle the
differences in API/behavioral issues caused by the complexity and
diversity of different cellular modem/WiFi vendors. Most of the
functionality is exposed on a D-Bus API, allowing other tools to use the
functionality provided by MoxaConnectionManager.

libmcm provides C language bindings for functionality provided by
MoxaConnectionManager, optionally useful from other language runtimes as
well.

libmcm converts the D-Bus API provided by MoxaConnectionManager into an
API that is easier for C developers to use, allowing customers to
operate without knowing D-bus. The package also contains sample code
that describes how to use libmcm.
