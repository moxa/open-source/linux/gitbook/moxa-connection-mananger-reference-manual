---
title: Interface Datausage
---

``` c
#include <libmcm/mcm-datausage.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(void) {
    printf("USAGE: \n");
    printf("datausage [Interface Name] <Start Time> <End Time>\n");
    printf("\tStart: \"YYYY-MM-DD HH:MM\" or \"YYYY-MM-DD\"\n");
    printf("\tEnd: \"YYYY-MM-DD HH:MM\" or \"YYYY-MM-DD\"\n");
    printf("datausage reset [Interface Name]\n");
}

int datausage(char *interface_name, char *start_time, char *end_time) {
    int                ret = 0;
    mcm_datausage_info datausage_info;
    memset(&datausage_info, 0, sizeof(mcm_datausage_info));

    if (!start_time && !end_time)
        ret = mcm_datausage_get_iface_total(interface_name, &datausage_info);
    else if (start_time && !end_time)
        ret = mcm_datausage_get_iface_from(interface_name, start_time, &datausage_info);
    else if (start_time && end_time)
        ret = mcm_datausage_get_iface_period(interface_name, start_time, end_time, &datausage_info);

    printf("Interface:%s\n", interface_name);
    printf("Tx: %d byte\n", datausage_info.tx);
    printf("Rx: %d byte\n", datausage_info.rx);
    return ret;
}

int main(int argc, char *argv[]) {
    int ret = 0;
    if (argc < 2) {
        usage();
        return -1;
    }

    if (argc == 3 && !strcmp(argv[1], "reset"))
        return mcm_datausage_reset(argv[2]);

    if (argc == 4)
        ret = datausage(argv[1], argv[2], argv[3]);
    else if (argc == 3)
        ret = datausage(argv[1], argv[2], NULL);
    else if (argc == 2)
        ret = datausage(argv[1], NULL, NULL);
    else
        usage();
    return ret;
}
```

------------------------------------------------------------------------
