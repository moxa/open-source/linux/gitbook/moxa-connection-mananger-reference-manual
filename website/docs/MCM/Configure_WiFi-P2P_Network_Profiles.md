---
title: Configure WiFi-P2P Network Profiles
---

### Select Network Profile:

Configure profile-1, including SSID/Password related connection settings

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531356.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531356.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531356"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231123-103849.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531152"
data-linked-resource-container-version="2" width="760" />

### Configure WiFi-P2P Setting

-   SSID

WiFi-P2P SSID name. The default value is `SerialNumber`.
DIRECT-*Serialnumber* or DIRECT-*SSID* will be displayed.

-   Password

WiFi-P2P password

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531448.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531448.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531448"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231123-104027.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531152"
data-linked-resource-container-version="2" width="760" /><img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531837.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531837.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531837"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231123-105318.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531152"
data-linked-resource-container-version="2" width="361" />

### Configure IP Method for WiFi

Only support IPv4 Method.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531769.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531152/249531769.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531769"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231123-104945.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531152"
data-linked-resource-container-version="2" width="760" />

### **Configure IPv4 reloated setting**

-   IPv4 address:

IPv4 address of WIFI-P2P interface.

-   IPv4 Subnet address:

IPv4 netmask of WIFI-P2P interface.


