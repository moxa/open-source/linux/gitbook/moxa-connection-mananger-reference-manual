---
title: WiFi D-bus Properties
---

|  |  |
|----|----|
| **Cellular Interface** | **Description** |
| common |  |
| "Name" | The name of the interface. |
| "DeviceType" | The interface type: Ethernet/WiFi/Modem |
| "DeviceName" | The device name of the interface. |
| "Enabled" | If the interface is managed. |
| "Network" | The interface works as WAN or LAN. |
| "WANPriority" | The interface's link priority when works as WAN. |
| "DefaultRoute" | If the interface is current default route. |
| "Profiles" | The list of profile names of the interface. (e.g Profile1, Profile2) |
| "ProfileName" | The current profile Name of the interface. ( \>= Version 1.3) |
| "MACAddress" | The MAC address of the interface. |
| "IPV4AddressMethod" | The method to get IPV4 address for the interface. (static or DHCP) |
| "IPV4Address" | The IPV4 address of the interface. |
| "IPV4Netmask" | The IPV4 netmask of the interface. |
| "IPV4Gateway" | The IPV4 gateway of the interface. |
| "IPV4PrimaryDNS" | The primary DNS server of the interface. |
| "IPV4SecondaryDNS" | The secondary DNS server of the interface. |
| "IPV6AddressMethod" | The method to get IPV6 address for the interface. ( only support ‘auto’) |
| "IPV6Address" | The IPV6 address of the interface. |
| "IPV6Netmask" | The IPV6 netmask of the interface. |
| "IPV6Gateway" | The IPV4 gateway of the interface. |
| "IPV6PrimaryDNS" | The primary DNS server of the interface. |
| "IPV6SecondaryDNS" | The secondary DNS server of the interface. |
| "ConnectionState" | The connection checking status of the interface. |
| "ErrorReason" | Error log when executing the interface ( \>= Version 1.3) |
| WiFi special |  |
| "OperationMode" | The Wifi interface mode. (only support ‘Client’ mode) |
| "Frequency" | The frequency of the connected Wifi AP. (2.4G or 5G) |
| "Channel" | The channel of the connected Wifi AP. (1.2….. etc) |
| "BSSID" | The BBSID of the connected Wifi AP. |
| "SSID" | The SSID Name of the connected Wifi AP. |
| "HWMode" | The HW mode of the connected Wifi AP. (802.11a or 802.11b/g) |
| "SecureMode" | The security mode of the connected Wifi AP. (WPA, WPA2) |
| "Signal" | The signal stregth of the connected Wifi AP. (e.g 20%) |
| "Broadcast" | Whether the connected Wifi AP is broadcast |
| "APList" | List of surrounding wifi APs |
