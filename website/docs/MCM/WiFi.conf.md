---
title: WiFi.conf
---

### Moxa Connection Manager (MCM) -- WiFi interface configure file

### Location

``` java
/etc/moxa/MoxaConnectionManager/interfaces/WiFi<ID>.conf
```

### Description

WiFi.conf is the configuration file of this WiFi interface. It contains
an `Interface` section to control the priority list of profiles and
timeout, and also contains the different `Profile` sections. Each
`Profile` section is the network settings for the WiFi interface ,
including WiFi AP information , ip usage method and how to judge that
the interface is connected.

### File Format

The configuration file format is so-called key file (sort of ini-style
format). It consists of sections (groups) of key-value pairs. Lines
beginning with a '#' and blank lines are considered comments. Sections
are started by a header line containing the section enclosed in '\[' and
'\]', and ended implicitly by the start of the next section or the end
of the file. Each key-value pair must be contained in a section.

The main section structure is as follows:

``` java
[Interface] 
    ...
[Profile-1]
    ...
[Profile-2]
    ...
[Profile-x]    
```

### Interface Section

-   `activated=true|false`

Set this interface as active or inactive. When this interface is set as
`WAN`, `LAN`, or `Manual `type, setting it as inactive will cause MCM to
automatically ignore this interface, avoiding connections, keepalives,
failovers, and failbacks… etc.

-   `connection.always-keep-alive=true|false`

You can enable this setting if a seamless failover experience is
required, meaning if a backup interface is set to always keep-alive,
then MCM can failover to a ready-to-use backup connection without the
initialization downtime.

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   `profile.retry-threshold=<TIMEOUT>`

This value (in counts) determines the maximum attempts MCM will try to
connect using a network profile before failover to the next profile in
the priority list

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   `profile.wait-connected-timeout=<TIMEOUT>`

This value (in seconds) determines the maximum time MCM will try to
connect using a network profile before determining the connection is
unavailable.

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   `profile.failover-priority=<PROFILE ID1>,<PROFILE ID2>`

The network profile numbers in order of priority. For example, enter 1,3
means MCM will use Profile-1 to connect and failover to Profile-3 when
Profile-1 cannot connect or become unavailable.

> :warning: this option is only available when the interface is set to
> the `failover.interface.priority` field.

-   led.signal.nosignal.exec= (v1.6 or above)
-   led.signal.verypool.exec=
-   led.signal.pool.exec=
-   led.signal.fail.exec=
-   led.signal.good.exec=
-   led.signal.excellent.exec=

When the signal strength changes to this level, execute the additional
hook.

-   dhcp-server.enabled=true (v1.6 or above)

When WiFi is set to LAN type (WiFi AP), always set `dhcp-server.enabled`
to `true`

### Profile-\[ID\] Section

> :warning: when interface is set to `standalone.interface.list` or
> `manual.interface.list` , MCM will only use **Profile-1** as
> networking setting.

-   `ip-method=IPV4|IPV6|IPV4V6`

When set to `IPV4V6`, the interface will obtain ipv4 address and ipv6
address from the base station, otherwise when set to `IPV4` or `IPV6`,
only ipv4 address or ipv6 address will be obtained.

-   `ipv4-method=static|dhcp`

    When `ipv4-method` is set to `dhcp`, `ipv4-address` `ipv4-netmask`
    ... etc. are obtained from the dhcp server.

    when `ipv4-method` is set to `static`, the other settings are as
    follows.

    -   `ipv4-address=<IPv4 Address>`

        This interface ipv4 address.

    -   `ipv4-netmask=<NETMASK>`

        This interface ipv4 netmask.

    -   `ipv4-gateway=<GATEWAY>`

        This interface ipv4 gateway.

    -   `primary-dns=<DNS Server>`

        This interface primary dns address.

    -   `secondary-dns=<DNS Server>`

        this interface secondary dns address.

-   `ipv6-method=auto`

Currently only support Stateless DHCPv6.

-   `Mode=client|AP (v1.6 or above)`

Set the WiFi interface to either client mode or AP mode. 

-   `SSID=<SSID>`

The SSID of the WiFi AP you want to connect to or the SSID to set when
configuring AP mode.

-   `Password=<AP Password>`

-   APBand=2.4GHz

Select the 2.4GHz band to be used as the WiFi AP band

-   `connection.check-method=ping | check-ip-exists | ping-signalmonitor | check-ip-exist-signalmonitor`

#### Ping Method

Use the `ping` command to check if the interface network is connected.

-   `ping.ipv4-target-host=<SERVER IPv4 Address>`

The ipv4 address of the target host

-   `ping.ipv6-target-host=<SERVER IPv6 Address>`

The ipv6 address of the target host

-   `ping.timeout=<TIMEOUT>`

This value (in seconds) determines the maximum time MCM will try to ping
the configured target host and wait for a response before determining
the connection is unavailable.

-   `connection.check-alive-interval=<INTERVAL>`

This value (in seconds) determines how often MCM check the connection is
alive.

-   `connection.check-failed-threshold=<THRESHOLD>`

This value determines the maximum attempts MCM will check the connection
status before concluding the connection is not available

#### Check-ip-exist Method

Check whether the machine has obtained the IPv4/IPv6 address. No
additional network traffic will be generated.

#### Ping-signalmonitor Method

Use the ***ping*** command to check whether the connection between the
machine and the server is normal. And check whether the WiFi signal
strength is greater than Threshold. If not, it means that the WiFi
transmission speed and stability are poor.

-   Please refer to the ping method for ping-related settings.

-   `signal.monitor-long-interval=<INTERVAL>`

This value (in seconds) determines how frequently MCM checks if the WiFi
signal exceeds the threshold.

-   `signal.monitor-short-interval=<INTERVAL>`

When MCM detects that the WiFi signal quality is below the threshold,
this value (in seconds) determines the retry interval.

-   `signal.monitor-short-interval-count=<COUNT>`

This value determines the maximum number of retry attempts before
concluding that the WiFi signal does not meet the specified threshold.

-   `signal.monitor-threshold-type=SIGNAL`

When configured as `SIGNAL`, MCM will use the signal strength percentage
(0-100) as the threshold. Users can utilize the 'mx-connect-mgmt
nwk_info WiFiX' command to retrieve the current signal strength
percentage.

-   `signal.monitor-threshold=<Percentage>`

Signal strength percentage (0-100) .

#### Check-ip-exist-signalmonitor Method

Check whether the machine has obtained the IPv4/IPv6 address. No
additional network traffic will be generated. And check whether the
Cellular signal strength is greater than Threshold. If not, it means
that the cellular transmission speed and stability are poor.

-   `signal.monitor-long-interval=<INTERVAL>`

This value (in seconds) determines how frequently MCM checks if the
Cellular signal exceeds the threshold.

-   `signal.monitor-short-interval=<INTERVAL>`

When MCM detects that the cellular signal quality is below the
threshold, this value (in seconds) determines the retry interval.

-   `signal.monitor-short-interval-count=<COUNT>`

This value determines the maximum number of retry attempts before
concluding that the cellular signal does not meet the specified
threshold.

-   `signal.monitor-threshold-type=SIGNAL`

When configured as `SIGNAL`, MCM will use the signal strength percentage
(0-100) as the threshold. Users can utilize the '***mx-connect-mgmt
nwk_info WiFiX'*** command to retrieve the current signal strength
percentage.

-   `signal.monitor-threshold=<Percentage>`

Signal strength percentage (0-100) .
