---
title: GPS
---

Users can configure GPS default on/off via
<a href="https://wiki.moxa.com/display/RDMCM/Configure+GPS+function"
rel="nofollow">GUI</a>.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531755.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531755.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531755"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-084518.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530999"
data-linked-resource-container-version="7" width="760" />

MCM also supports CLI commands to enable or disable GPS at execution
time.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531615.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531615.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531615"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-084821.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530999"
data-linked-resource-container-version="7" width="760" />

### **Turn On GPS**

``` java
#mx-connect-mgmt GPS <Interface> on
```

### Turn Off GPS

``` java
#mx-connect-mgmt GPS <Interface> off
```

### Get GPS State

``` java
#mx-connect-mgmt GPS <Interface> get_state
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531281.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531281.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531281"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-085037.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530999"
data-linked-resource-container-version="7" width="760" />

### Use mx-interface-mgmt command to get GPS Port

``` java
---
title: mx-interface-mgmt cellular <interface>
---
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531789.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530999/249531789.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531789"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-085123.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530999"
data-linked-resource-container-version="7" width="764" />

## Control GPSD Service

> :warning: only supported v1.4.0 or above

MCM automatically start ***GPSD service*** when gps function default
[enabled](Configure_GPS_function). And the GPS port will be
automatically updated to ensure that correct GPS data can be obtained.


