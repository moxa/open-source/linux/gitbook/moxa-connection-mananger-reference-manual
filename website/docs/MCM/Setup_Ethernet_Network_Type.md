---
title: Setup Ethernet Network Type
---

## Select Ethernet interface

After selecting the ethernt interface, the settings related to the
ethernet connection will begin.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531607.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531607.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531607"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-104911.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="760" />

## Network Type Setting

This page mainly sets information related to Ethernet connection.

> :information_source: The default network type for LAN1 is \[WAN\] and
> for LAN2 is \[None\]

Select Ethernet network type as `WAN/LAN/LAN-Bridge/Manual/None`type.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531367.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531367.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531367"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-105140.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="760" />

-   **WAN**:

After being configured as a WAN. The Ethernet interface will be added to
the ***failover priority list***, that is, it will have a keep-alive
function. The MCM will keep Ethernet active and configure the default
gateway according to the priority list. Please reference the
Failover/Failure chapter.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531473.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531473.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531473"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-105250.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="764" />

-   **Manual**

MCM only helps establish connections, but it does not have keepalive and
failover/failback capabilities. This is a one-time connection. By
default, only Profile-1 can be used.

MCM will not set these interfaces as the default route.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531482.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531482.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531482"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-105313.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="760" />

-   **LAN**

Defined as a local network type, no default route will be configured.
Because it is not a WAN type, it does not have the keep-alive function.
Profile-1 is used by default.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531739.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531739.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531739"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-105420.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="760" />

**DHCP Server** can be enabled for this interface。

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531491.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531491.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531491"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-110807.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="642" />

> :warning: When enabling the dhcp server, the ipv4 address set in
> profile1 needs to be within the dhcp range.

-   **LAN-Bridge**

This setup typically involves connecting multiple Ethernet networks to a
bridge and creating a larger LAN.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531793.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531793.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531793"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-105533.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="760" />

**DHCP Server** can be enabled for this interface。

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531286.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531117/249531286.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531286"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-110726.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531117"
data-linked-resource-container-version="5" width="636" />

> :warning: When enabling the dhcp server, the bridge ipv4 address needs
> to be within the dhcp range.

-   **None**

MCM does not control Ethernet interface


