---
title: Time Synchronization
---

> :warning: Only supports v1.4.0 or above

MCM will automatically update the system clock according to the method
selected by the user.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530879/249531337.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530879/249531337.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531337"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231124-081209.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530879"
data-linked-resource-container-version="5" width="760" />

### Configure Auto Timesync Method

MCM currently supports 4 methods:

-   Disabled (default)

Turnoff the auto time-sync function.

-   GPS

Update system clock by using GPS time. Note that GPS antenna needs to be
installed and GPS function needs to be turned on. Please referece [GPS
section](GPS) to turn on GPS.

-   Chrony

Use Chrony service to ask the NTP server to obtain the current system
clock.

-   Cellular

Update system clock by using Cellular Basestation time. Please note that
a cellular connection is required to obtain the time. Please referece
‘[Setup Cellular Connection](Setup_Cellular_Connection) section’ to
create cellular connection.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530879/249531317.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530879/249531317.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531317"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231124-081527.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530879"
data-linked-resource-container-version="5" width="760" />

### Configure Auto Timesync Interval

Set the interval for updating the system time.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530879/249531677.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530879/249531677.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531677"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231124-084849.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530879"
data-linked-resource-container-version="5" width="760" />


