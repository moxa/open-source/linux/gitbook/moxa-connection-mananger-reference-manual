---
title: Configure Multi Backup WAN interfaces
---

One of the main functions of MCM is automatic failover of WAN
interfaces, which allows users to configure multiple interfaces to
backup WAN networks. When the primary interface is disconnected, MCM
automatically failover to other priority backup networks. When the
primary network is restored, it will failback to the higher priority
network interface.

### Examples: configure Cellular interface as WiFi interface backup

In *“Configure WAN interface failover priority”* settings, select first
priority as the WiFi interface and second priority as the Cellular
interface. When the wifi interface is disconnected, MCM will
automatically switch the default route to the cellular interface
(**failover process**). When the WiFi connection is restored and the
connection remains stable for `failback check interval`seconds, the
default route will automatically switch to WiFi interface (**failback
process**) .

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531229/249531302.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531229/249531302.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531302"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-030723.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531229"
data-linked-resource-container-version="6" width="772" />

### Connection process:

#### a) Normal Failover/Failback (without Profile informaction)

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531711"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-075450.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531074"
data-linked-resource-container-version="3" width="760" />

#### a) Normal Failover/Failback (with Profile informaction)

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531073/249531673.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531073/249531673.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531673"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-081028.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531073"
data-linked-resource-container-version="2" width="760" />


