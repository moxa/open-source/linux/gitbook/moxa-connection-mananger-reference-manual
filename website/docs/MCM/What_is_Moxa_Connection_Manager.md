---
title: What is Moxa Connection Manager
slug: /
---

Given the complexity and variety among different cellular modem vendors,
we propose implementing a unified layer to address the discrepancies in
APIs and behaviors. By leveraging the open-source resources from
<a href="http://Freedesktop.org"
rel="nofollow">http://Freedesktop.org</a> 's Modem Manager (MM) and
Network Manager (NM) projects, we aim to enhance the user experience for
our customers. Our scope includes handling pure cellular modem
communication requirements, managing network interfaces such as
Ethernet, cellular modems, 802.11abgn Wi-Fi, VPNs, and routers, and
directly interfacing with NM/MM/routers to handle event notifications
through Moxa's proprietary software stack.

As illustrated in the figure below, Moxa Connection Manager will control
Wi-Fi, 3G/4G/5G, and Ethernet interfaces. These interfaces can be used
as WAN connections to external clouds or to gather sensor information.
Currently, Ethernet is the primary connection method for gathering
sensor information.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530909/249531780.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530909/249531780.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531780"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20240614-072055.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530909"
data-linked-resource-container-version="6" width="760"
alt="image-20240614-072055.png" />


