---
title: Module Firmware Upgrade
---

MCM will check and install the latest cellular modem firmware from Moxa
APT servers. These firmwares are certified and tested by moxa.

``` java
---
title: mx-connect-mgmt modem upgrade [Interface name] 
---
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531469.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531469.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531469"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-073814.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530892"
data-linked-resource-container-version="2" width="760" />

### Upgrade process:

During the module firmware process, the Cellular network will be
disconnected. After Modem Upgrade, whether the Cellular connection will
be restored depends on whether the Cellular status before the upgrade
was "Disabled" (if the modem was originally disabled, it will not be
reconnected, but if it is not disabled, it will be reconnected).

Example:

-   WiFi→ Cellular and Seamless feature disabled

When the default route is still on the WiFi interface and Cellular is
not to connect (that is, it is in disabled status), the connection will
not be restored after the upgrade.

-   WiFi→ Cellular，Seamless feature enabled

Although the default route is still on the WiFi interface, since
Cellular is connected at the same time (that is, in non-disabled
status), the cellular connection will be restored after the upgrade.

### Automatically updating the lastest cellular modem firmware

``` java
#mx-connect-mgmt modem upgrade <Cellular_Interface>
```

### manually updating the cellular modem firmware by specifying a firmware file

``` java
#mx-connect-mgmt modem upgrade <Cellular_Interface> -F <firmware info path>
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531624.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531624.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531624"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-074701.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530892"
data-linked-resource-container-version="2" width="760" />

### Force Upgrade FWR

``` java
#mx-connect-mgmt modem upgrade -f <Cellular_Interface>
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531444.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531444.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531444"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-074747.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530892"
data-linked-resource-container-version="2" width="760" />

Firmware Downgrade is not recommended. When automatically updating the
modem firmware, it will check whether the module FWR is up to date to
decide whether to continue the update. But if for some special reasons,
you want Force to refresh the module firmware, you can do it through the
force command.

### Check the current cellular modem firmware version

``` java
#mx-connect-mgmt nwk_info -a <Cellular_Interface>
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531693.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249530892/249531693.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531693"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-075014.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249530892"
data-linked-resource-container-version="2" width="760" />


