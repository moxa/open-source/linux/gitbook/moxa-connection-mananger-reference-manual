---
title: Basic Control
---

### Check whether the moxa-connection-manager service is started (enabled by default).

``` java
systemctl status moxa-connection-manager
```

### List all available interfaces

To check which modules are inserted into the machine and which devices
are supported, you can query through the following command

``` java
mx-connect-mgmt ls
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531007/249531542.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531007/249531542.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531542"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-095336.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531007"
data-linked-resource-container-version="4" width="764" />

### Edit the interfaces you want to use

There are two ways to edit interface settings

1.  Use the GUI dialog : [GUI](GUI)

``` java
#mx-connect-mgmt configure
```

1.  Directly edit the configuartion file [Configuration file
    structure](Configuration_file_structure)

### Reload the MCM configuartion files

-   Using GUI

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531007/249531648.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531007/249531648.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531648"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-095902.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531007"
data-linked-resource-container-version="4" width="739" /><img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531007/249531815.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531007/249531815.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531815"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-095922.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531007"
data-linked-resource-container-version="4" width="739" />

-   Edit configureation file method

Users can reload the configuration file and MCM through the
`reload `command

``` java
#mx-connect-mgmt reload 
```

### Or restart MCM,all interfaces will restart

``` java
#mx-connect-mgmt stop
#mx-connect-mgmt start
```

## Using CLI to display current interface status

[Network Interface Status](Network_Interface_Status)

``` java
#mx-connect-mgmt nwk_info <Interface>
```


