---
title: Unlock SIM PIN/PUK
---

MCM CLI command supports the function of manually unlock PIN codes or
PUK codes. Users can check if the sim pin is correct and unlock the sim
card.

### unlock_pin

``` java
mx-connect-mgmt unlock_pin <Cellular_Interface> <PIN_Code>
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531236/249531671.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531236/249531671.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531671"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-061003.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531236"
data-linked-resource-container-version="2" width="760" />

In order to avoid affecting the interface connection, it is necessary to
stop MCM to unlock the PIN.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531236/249531435.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531236/249531435.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531435"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-061334.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531236"
data-linked-resource-container-version="2" width="763" />

> :information_source: When there is only one SIM PIN remaining times,
> MCM will not try to unlock the PIN to prevent the SIM from entering
> the PUK state.

> :information_source: After MCM attempts a wrong PIN once, it will stop
> the cellulr connection to avoid trying multiple wrong PIN.

### **unlock_puk**

``` java
#mx-connect-mgmt unlock_puk <Cellular_Interface> <PUK_Code> <PIN_Code>
```

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531236/249531743.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531236/249531743.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531743"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231113-062649.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531236"
data-linked-resource-container-version="2" width="760" />

In order to avoid affecting the interface connection, it is necessary to
stop MCM to unlock the PUK.


