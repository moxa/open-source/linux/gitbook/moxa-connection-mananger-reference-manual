---
title: CLI
---

### Command help

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531188/249531724.png" class="image-left"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531188/249531724.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531724"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231108-080450.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531188"
data-linked-resource-container-version="15" width="624" />

### Command List:

|  |  |  |  |
|----|----|----|----|
| **Command** | **Subcommand** | **Version** | **Description** |
| [GPS ](GPS) | -- | v1.3 or above | Control and get GPS |
| [configure](GUI) | -- |  | Configure MOXA Connection Management via GUI dialog |
| [datausage](Data_Usage)  | -- |  | Show interface data usage. |
| [default](Reset_to_default) | -- | v1.3 or above | Reset all configuration to default setting for MCM |
| debug | [Diagnosis](Cellular_Disagnosis) |  | Diagnose cellular signal environment and profile settings |
| ls | -- |  | Lists network interfaces. |
| modem | [upgrade](Module_Firmware_Upgrade) |  | Upgrade module firmware |
| [nwk_info](Network_Interface_Status) | -- |  | View the interface's connection status and information. |
| reload | -- |  | Reload configuration files and restart the connection. |
| start/stop | -- |  | Start/stop all connections |
| [unlock_pin](Unlock_SIM_PIN_PUK) | -- |  | Manually unlock SIM Pin. |
| [unlock_puk](Unlock_SIM_PIN_PUK) | -- |  | Manually unlock SIM PUK. |
| wifi | ap_list |  | Show WiFi AP List |
|  | rescan |  | Rescan Wi-Fi AP |


