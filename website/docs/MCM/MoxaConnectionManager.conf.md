---
title: MoxaConnectionManager.conf
---

### Moxa Connection Manager (MCM) -- main configuration file

### Location

``` bash
/etc/moxa/MoxaConnectionManager/MoxaConnectionManager.conf
```

### Description

`MoxaConnectionManager.conf` is the main configuration file for Moxa
Connection Manager (MCM). The configuration includes a `log-level` field
to control the verbosity of logging. Also includes
`manual.interface.list`, `failover.interface.priority`,
`standalone.interface.list`, `bridge.interface.list` fields to set the
interface type to `WAN`, `LAN`, or `manual`.

### File Format

The configuration file format is so-called key file (sort of ini-style
format). It consists of sections (groups) of key-value pairs. Lines
beginning with a '#' and blank lines are considered comments. Sections
are started by a header line containing the section enclosed in '\[' and
'\]', and ended implicitly by the start of the next section or the end
of the file. Each key-value pair must be contained in a section.

The main section structure is as follows:

``` c
[Main] 
    ...
[WAN]
    ...
[LAN]
    ...
```

### Main Section

-   `log-level=<level>`

The default logging verbosity level. One of `ERR`, `WARN`, `INFO`,
`DEBUG`, `TRACE`. The ERR level logs only critical errors. WARN logs
warnings that may reflect operation. INFO logs various informational
messages that are useful for tracking state and operations. DEBUG
enables verbose logging for debugging purposes. TRACE enables even more
verbose logging then DEBUG level. Subsequent levels also log all
messages from earlier levels; thus setting the log level to INFO also
logs error and warning messages.

-   `manual.interface.list=<interface_1>,<interface_2>,... <interface_x>`

When the interface is set to the `manual.interface.list`field that
allows the user to have total control over this interface. MCM will
connect this interface one-time only network attributes defined in
Profile-1. MCM will not set these interfaces as default gateway nor
apply connection keep-alive and failover/failback control over it.

> :warning: The interface will use ***Profile-1*** as the default
> network setting, even if `profile.failover-priority` fileld has other
> settings.

-   `multi.wan.interface.list=<interface_1>,<interface_2>,... <interface_x> (v1.6 or above)`

 When set to Multi-WAN, this interface will be NOT added to the default
gateway list but allow MCM to apply automatic keep-alive. Users can
access and control the machine through this interfaces without affecting
default WAN interfaces.

-   `auto-gpsd-sync=true|false `(v1.4 or above)

The MCM keepalive function may reset the modem, potentially causing
changes to the GPS port. When set to '`true`', MCM will automatically
synchronize the updated GPS port with the `GPSD `service, allowing users
to directly obtain the current GPS location via the GPSD service.

-   `auto-timesync=Disabled/Chrony/GPS/Cellular` (v1.4 or above)

Set the auto timesync method. By default, it is `disabled`, meaning
timesync is not activated. When set to `GPS`, the system date will be
updated using timestamps from GPS satellites. When set to `Cellular`,
the system time will be updated using the base station's time. When set
to `Chrony`, the system date will be obtained via a chrony server (NTP).

-   `auto-timesync.interval=<interval>` (v1.4 or above)

This value determines the frequency (in seconds) of updating the system
date.

-   `auto-timesync.interface=Cellular1` (v1.4 or above)

Because Moxa devices can simultaneously use more than one cellular
module, when `auto-timesync` is set to `GPS `or `Cellular`, you need to
specify which module to use as the data input.By default, it uses the
first cellular module (Cellular1).

-   `modules.initialize.exec` (v1.5 or above)

Command Executed at MCM Startup. e.g 'modules.initialize.exe= bash -c
mx-module-ctl -s 1 -p on '

-   `modules.finalize.exec` (v1.5 or above)

Command Executed When MCM Stop. e.g 'modules.initialize.exe= bash -c
mx-module-ctl -s 1 -p off'

### WAN Section

-   `failover.interface.priority=<Priority_1>,<Priority_2>,... <Priority_x> `

When set to WAN, this interface will be added to the default gateway
list and allow MCM to apply automatic keep-alive and failover/failback
control over it.

failover: MCM will use the WAN interface set to 1st Priority as the
default gateway. When the 1st priority interface becomes unavailable,
MCM will automatically failover to the next priority interface.

failback: The backup connection will automatically failback to the
higher priority connection when it became available again.

-   `failback.enabled=true|false`

Enabled failback feature.

-   `failback.check-interval=<interval>`

This value determines how long (in seconds) the higher priority
connection should maintain stability before MCM trigger the failback.
The purpose is to avoid unstable connections causing frequent failover
and failback.

> :information_source: When the value set to '0' , MCM will immediately
> switch to the high priority interface which is connected.

-   `global.dns.servers=<DNS Server>` (v1.3 or above)

Users can additionally add DNS servers to all interfaces.

> :warning: because the gateway for Cellular is on an external network,
> it's possible that the DNS server won't function due to the gateway
> being unable to locate additional DNS servers.

### LAN Section

-   `standalone.interface.list=<interface_1>,<interface_2>,... <interface_x>`

MCM will connect this interface using the network attributes defined in
Profile-1 and DHCP server can be enabled for this interface.

> :warning: The interface will use ***Profile-1*** as the default
> network setting, even if `profile.failover-priority` fileld has other
> settings.

-   `bridge.interface.list=<interface_1>,<interface_2>,... <interface_x>`

Bridge two or more LAN interfaces to construct a larger LAN. The bridge
interfaces will only use the following settings instead of the
*Profile-x* settings in *\<interface_name\>*.conf.

-   `ipv4-address=<IP Address>`

The bridge interface ipv4 address.

-   `ipv4-netmask=<Netmask>`

The bridge interface ipv4 netmask.

-   `dhcp-server.enabled=true|false`

    -   `dhcp-server.ip-lease-range=<Start IP Address>, <End IP Address>`

        The range must belong to the subnet of the bridge interface. For
        example, when `ipv4.address` filed is set to 192.168.0.127 and
        `ipv4-netmask` is set to 255.255.255.0, then the dhcp ip range
        needs to be a subset of 192.168.0.1 to 192.168.0.254. or when
        `ipv4.address` filed is set to 192.168.0.127 and `ipv4-netmask`
        is set to 255.255.0.0, then the dhcp ip range needs to be a
        subset of 192.168.0.1 to 192.168.254.254.

    -   `dhcp-server.ip-lease-time=<TIME>`

        If the lease time is given, then leases will be given for that
        length of time. The lease time is in seconds, or minutes (eg
        45m) or hours (eg 1h) or days (2d) or weeks (1w) or "infinite".
        If not given, the default lease time is one hour for IPv4. The
        minimum lease time is two minutes
