---
title: Configure WiFi WAN Network Profiles
---

### Select Network Profile:

Configure each profile, including SSID/Password/Check Alive related
connection settings

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531836.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531836.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531836"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-101645.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531019"
data-linked-resource-container-version="6" width="764" />

### Configure WiFi Setting

-   SSID

SSID name of WiFi AP

-   Password

WiFi AP password

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531740.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531740.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531740"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-102047.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531019"
data-linked-resource-container-version="6" width="760" />

### Configure IP Method for WiFi

The default is IPv4, set the IP address method users want to connect to.
When set to IPv4v6, the interface will obtain IPv4 address and IPv6
address from the base station, otherwise when set to IPv4 or IPv6, only
IPv4 address or IPv6 address will be obtained.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531486.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531486.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531486"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024505.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531019"
data-linked-resource-container-version="6" width="760" />

### **Configure Connection Check-alive Method**

Select how to determine whether the connection is successful. Currently
two methods are supported: ping and check-ip-exist.

-   ping (default):

Use the ***ping*** command to check whether the connection between the
machine and the server is normal.

-   check-ip-exist

Check whether the machine has obtained the IPv4/IPv6 address. No
additional network traffic will be generated.

> :information_source: check-ip-exist method is only supported by v1.3
> and above.

-   ping-signalmonitor

Use the ***ping*** command to check whether the connection between the
machine and the server is normal. And check whether the Cellular signal
strength is greater than Threshold. If not, it means that the cellular
transmission speed and stability are poor.

-   check-ip-exist-signalmonitor

Check whether the machine has obtained the IPv4/IPv6 address. No
additional network traffic will be generated. And check whether the
Cellular signal strength is greater than Threshold. If not, it means
that the cellular transmission speed and stability are poor.

> :information_source: \*-signalmonitor method is only supported by v1.4
> and above.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531574.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531574.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531574"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231124-090258.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531019"
data-linked-resource-container-version="6" width="760" />

### **Configure Ping method settings**

-   IPv4 Ping Target Host :

The default value is 8.8.8.8. If you use IPv4/IPv4v6 to connect, use the
target host to check the connection. Domain names are also supported.

> :information_source: Domain names feature only supported by v1.3 and
> above.

-   IPv6 Ping Target Host:

If connecting using the IPv6 IP method, this target host will be used to
check the connection.

-   Ping Timeout:

The default is 3 seconds, indicating the timeout of the ping command.

-   Configure Check-alive Check Interval :

The default is 300 seconds, which means the ping method will be checked
for connectivity every 300 seconds.

-   Configure Check-alive Retry Threshold :

The default is 3, which means each check-alive will ping 3 times to
check for connectivity.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531600.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531600.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531600"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-024541.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531019"
data-linked-resource-container-version="6" width="760" />

### **Time-control for interface connection by using ping method**

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531074/249531711.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531711"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231115-075450.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531074"
data-linked-resource-container-version="3" width="760" />

### **Configure Check-ip-exist method settings**

No need to configure other options.

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531410.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531410.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531410"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20231110-102529.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531019"
data-linked-resource-container-version="6" width="760" />

### **Configure \*-signalmonitor method settings**

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531506.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531019/249531506.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531506"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20240418-032027.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531019"
data-linked-resource-container-version="6" width="760"
alt="image-20240418-032027.png" />

-   Configure Signal Monitor Long Interval

This value (in seconds) determines how frequently MCM checks if the
Cellular signal exceeds the threshold.

-   Configure Signal Monitor Threshold Type

When configured as `SIGNAL`, MCM will use the signal strength percentage
(0-100) as the threshold. Users can utilize the '***mx-connect-mgmt
nwk_info WiFiX'*** command to retrieve the current signal strength
percentage.

-   Configure Signal Monitor Threshold

Signal strength percentage (0-100) .

<img src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531136/249531319.png" class="image-center"
draggable="false" data-image-src="/open-source/linux/gitbook/moxa-connection-manager-reference-manual/attachments/249531136/249531319.png"
data-unresolved-comment-count="0" data-linked-resource-id="249531319"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image-20230908-024906.png"
data-base-url="https://wiki.moxa.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="249531136"
data-linked-resource-container-version="1" width="846" />


