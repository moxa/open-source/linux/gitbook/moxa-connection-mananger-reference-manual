var structmcm__network__status =
[
    [ "connection_status", "structmcm__network__status.html#ae23b0791b194952a55a92820731a998d", null ],
    [ "default_route", "structmcm__network__status.html#aa6089a02ca87959574e86467e3e7fecb", null ],
    [ "ipv4_address", "structmcm__network__status.html#a5e5ba0dd94686af1f6279fe5edac1892", null ],
    [ "ipv4_dns", "structmcm__network__status.html#a06b64b4d2114d6df25a6a06c030427a5", null ],
    [ "ipv4_gateway", "structmcm__network__status.html#a1553cbed186cacfd0409340e64dd7c25", null ],
    [ "ipv4_netmask", "structmcm__network__status.html#a18943dc3ad64e5acbe8e7bc6817b7e90", null ],
    [ "ipv6_address", "structmcm__network__status.html#a4a655ad20866a05daacd4d2024b67a2e", null ],
    [ "ipv6_dns", "structmcm__network__status.html#aba6e95f01fc58cdc79394a1aec16d5c8", null ],
    [ "ipv6_gateway", "structmcm__network__status.html#a1e07a23f6e2e446ef2c33d9d74ea6137", null ],
    [ "ipv6_netmask", "structmcm__network__status.html#a90bfb32249407e20ecdc6861a34f46ab", null ]
];