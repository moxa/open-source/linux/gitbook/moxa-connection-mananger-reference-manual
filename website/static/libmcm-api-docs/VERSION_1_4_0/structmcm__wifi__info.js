var structmcm__wifi__info =
[
    [ "broadcast", "structmcm__wifi__info.html#a0dc265af3f01642016b6c2fea3f47b97", null ],
    [ "bssid", "structmcm__wifi__info.html#ac8681eef57136344df55f2cfd40ead34", null ],
    [ "channel", "structmcm__wifi__info.html#a4a8c8414c1cb3936442c91298a54cb83", null ],
    [ "frequency", "structmcm__wifi__info.html#a616a41aa35e271b13dde8f2538873f17", null ],
    [ "hwmode", "structmcm__wifi__info.html#a955deb41eace0d3e7bc08b90e5187b5e", null ],
    [ "operation_mode", "structmcm__wifi__info.html#af30a622f61d035104cb7486964cbebc7", null ],
    [ "secure_mode", "structmcm__wifi__info.html#a98f8506389f0e5da3b990656047404e4", null ],
    [ "signal", "structmcm__wifi__info.html#ac3d7ef6a7074b5af4e3b39318f4293d7", null ],
    [ "ssid", "structmcm__wifi__info.html#a2d5f29437ca14ce27bd3c90ca46938e7", null ]
];