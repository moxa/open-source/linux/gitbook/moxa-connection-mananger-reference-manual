var structmcm__wifi__ap =
[
    [ "bssid", "structmcm__wifi__ap.html#ada54e13a310979ac1b9cc5a0e99eda1c", null ],
    [ "encryption_type", "structmcm__wifi__ap.html#a227e10e3aefa6508882654e931f1e93e", null ],
    [ "frequency", "structmcm__wifi__ap.html#a2880357f0464e0838d0ca5ba03ebc4cf", null ],
    [ "rssi", "structmcm__wifi__ap.html#a2e1a04f7389253ee1a17c2b76027b641", null ],
    [ "signal_strength", "structmcm__wifi__ap.html#a0129252c298006d15b8110cac1e343fd", null ],
    [ "ssid", "structmcm__wifi__ap.html#af8e24790203a26af72faca89d5796eb3", null ]
];