var structmcm__network__info =
[
    [ "device_name", "structmcm__network__info.html#aa23031d671bb905f72186fe8edc4a377", null ],
    [ "device_type", "structmcm__network__info.html#a4f978dd9876acda2c1d25fa1191d3505", null ],
    [ "enabled", "structmcm__network__info.html#a8fad6727994d48c65035da33483fb35e", null ],
    [ "err_reason", "structmcm__network__info.html#a33de69fdf8aa465daabf0c16124958e6", null ],
    [ "ipv4_method", "structmcm__network__info.html#a632377fbe1f9ded216adf3dbd9145a45", null ],
    [ "ipv6_method", "structmcm__network__info.html#acad52ce45f13aa0339eedc4be9580852", null ],
    [ "mac_address", "structmcm__network__info.html#a7511185a2f1e9bcda5ce4517d06a2e12", null ],
    [ "name", "structmcm__network__info.html#a249f6684e118b42c7967507e9746a48d", null ],
    [ "network_type", "structmcm__network__info.html#aa3fe6ceda47f2ff6983dae7b071029f0", null ],
    [ "wan_priority", "structmcm__network__info.html#a7ab60becabbd7f70e7775875338d3ea0", null ]
];