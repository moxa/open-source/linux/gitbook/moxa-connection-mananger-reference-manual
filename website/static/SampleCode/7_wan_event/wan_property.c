#include <libmcm/mcm-listen-event.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int stop_flag = 1;

int get_cb(const char *property_name, const char *property_value) {
    printf("property_name: %s\n", property_name);
    printf("property_value: %s\n", property_value);
    return 0;
}

void sighandler(int signum) {
    printf("Caught signal %d, coming out...\n", signum);
    stop_flag = 0;
}

void listen_wan_property_changed() {
    event_context *context = mcm_init_property_changed_event_context();

    int ret = mcm_subscribe_properties_changed_wan(context, get_cb);
    if (ret != 0) {
        printf("ERROR: listen interface(%s) property cheanged failed :%d\n", ret);
    }

    mcm_start_listen_subscribe_properties_changed_event(context);

    while (stop_flag)
        ;

    mcm_stop_listen_subscribe_properties_changed_event(context);
}

int main(int argc, char *argv[]) {
    signal(SIGINT, sighandler);

    listen_wan_property_changed();

    return 0;
}
