#include <libmcm/mcm-base-info.h>
#include <libmcm/mcm-wifi-info.h>
#include <libmcm/mcm-wifi-rescan.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void usage(void) {
    printf("USAGE: info [Interface Name]\n");
}

int rescan_ap_list(char *interface_name) {
    return mcm_wifi_rescan(interface_name);
}

int show_ap_list(char *interface_name) {
    int          ret = 0;
    int          num;
    mcm_wifi_ap *apilist;

    ret = mcm_get_wifi_aplist(interface_name, &num, &apilist);
    if (ret != 0) {
        printf("ERROR: Get wifi ap list failed:%d\n", ret);
        return -1;
    }

    printf("Number:%d\n", num);

    for (int i = 0; i < num; i++) {
        printf("[%d]\n", i);
        printf("\tssid:%s\n", apilist[i].ssid);
        printf("\tbssid:%s\n", apilist[i].bssid);
        printf("\tEncryption:%s\n", apilist[i].encryption_type);
        printf("\tSignal Strength:%d\n", apilist[i].signal_strength);
    }
    free(apilist);
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        usage();
        return -1;
    }

    rescan_ap_list(argv[1]);
    sleep(5);
    return show_ap_list(argv[1]);
}
