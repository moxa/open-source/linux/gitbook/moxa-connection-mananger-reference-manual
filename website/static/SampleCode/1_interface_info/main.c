#include <libmcm/mcm-eth-info.h>
#include <libmcm/mcm-interface-info.h>
#include <libmcm/mcm-modem-info.h>
#include <libmcm/mcm-network-info.h>
#include <libmcm/mcm-wifi-info.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(void) {
    printf("USAGE: info [Interface Name]\n");
}

int show_ethernet_info(char *interface_name) {
    int ret = 0;

    mcm_ethernet_info ethernet_info;
    memset(&ethernet_info, 0, sizeof(mcm_ethernet_info));

    ret = mcm_get_ethernet_info(interface_name, &ethernet_info);
    if (ret != 0) {
        printf("ERROR: Get ethernet interface (%s) information failed:%d\n", interface_name, ret);
        return -1;
    }

    printf("Duplex:%s\n", ethernet_info.duplex);
    printf("Link Detecte:%s\n", (ethernet_info.link_detected == true) ? "true" : "false");
    printf("Link Speed:%d Mbps\n", ethernet_info.link_speed);
    return 0;
}

int show_wifi_info(char *interface_name) {
    int           ret = 0;
    mcm_wifi_info wifi_info;

    memset(&wifi_info, 0, sizeof(mcm_wifi_info));
    mcm_get_wifi_info(interface_name, &wifi_info);
    if (ret != 0) {
        printf("ERROR: Get wifi interface (%s) information failed:%d\n", interface_name, ret);
        return ret;
    }

    printf("Operation Mode:%s\n", wifi_info.operation_mode);
    printf("BSSID:%s\n", wifi_info.bssid);
    printf("SSID:%s\n", wifi_info.ssid);
    printf("HWMode:%s\n", wifi_info.hwmode);
    printf("Frequency:%s\n", wifi_info.frequency);
    printf("Secure Mode:%s\n", wifi_info.secure_mode);
    printf("Channel:%d\n", wifi_info.channel);
    printf("Broadcast:%s\n", (wifi_info.broadcast == true) ? "true" : "false");
    return 0;
}

int show_modem_info(char *interface_name) {
    int            ret = 0;
    mcm_modem_info modem_info;

    memset(&modem_info, 0, sizeof(mcm_modem_info));
    mcm_get_modem_info(interface_name, &modem_info);
    if (ret != 0) {
        printf("ERROR: Get modem interface (%s) information failed:%d\n", interface_name, ret);
        return ret;
    }

    printf("CellID:%s\n", modem_info.cell_id);
    printf("Device IMEI:%s\n", modem_info.device_imei);
    printf("Network Rat:%s\n", modem_info.network_rat);
    printf("Operator:%s\n", modem_info.operator);
    printf("SIM ICCID:%s\n", modem_info.sim_iccid);
    printf("SIM IMSI:%s\n", modem_info.sim_imsi);
    printf("TAC LAC:%s\n", modem_info.taclac);
    printf("PIN Retries:%d\n", modem_info.pin_retries);
    printf("Signal Strength:%d\n", modem_info.signal_strength);
    printf("SIM Slot :%d\n", modem_info.sim_slot);
    printf("Modem State :%d\n", modem_info.modem_state);
    printf("Modem Name :%s\n", modem_info.modem_name);
    printf("Modem Version :%s\n", modem_info.modem_version);

    return 0;
}

int network_info(char *interface_name) {
    int                ret = 0;
    mcm_network_info   network_info;
    mcm_network_status network_stauts;
    mcm_profiles      *profiles;
    int                num;

    if (!interface_name)
        usage();

    memset(&network_info, 0, sizeof(mcm_network_info));
    memset(&network_stauts, 0, sizeof(network_stauts));

    ret = mcm_get_network_info(interface_name, &network_info);
    if (ret != 0) {
        printf("ERROR: Get interface (%s) information failed:%d\n", interface_name, ret);
        return -1;
    }

    printf("Name:%s\n", network_info.name);
    printf("Device Type:%s\n", network_info.device_type);
    printf("Device Name:%s\n", network_info.device_name);
    printf("Enabled:%s\n", (network_info.enabled == true) ? "true" : "false");
    printf("WAN Priority:%d\n", network_info.wan_priority);
    printf("Network:%s\n", network_info.network_type);
    printf("IPV4 Address Method:%s\n", network_info.ipv4_method);
    printf("MAC Address:%s\n", network_info.mac_address);
    printf("-----------------------------------------------------\n");

    ret = mcm_get_network_status(interface_name, &network_stauts);
    if (ret != 0) {
        printf("ERROR: Get interface (%s) status failed:%d\n", interface_name, ret);
        return -1;
    }

    printf("Default Route:%s\n", (network_stauts.default_route == true) ? "true" : "false");
    printf("IPV4 Address:%s\n", network_stauts.ipv4_address);
    printf("IPV4 Netmask:%s\n", network_stauts.ipv4_netmask);
    printf("IPV4 Gateway:%s\n", network_stauts.ipv4_gateway);
    printf("IPV4 PrimaryDNS:%s\n", network_stauts.ipv4_dns[0]);
    printf("IPV4 SecondaryDNS:%s\n", network_stauts.ipv4_dns[1]);
    printf("Connection State:%d\n", network_stauts.connection_status);
    printf("-----------------------------------------------------\n");

    ret = mcm_get_network_profiles(interface_name, &num, &profiles);
    if (ret != 0) {
        printf("ERROR: Get interface (%s) profiles failed:%d\n", interface_name, ret);
        return -1;
    }
    printf("Profiles:\n");
    for (int i = 0; i < num; i++) {
        printf("\t%s\n", profiles[i].profile_name);
    }
    printf("-----------------------------------------------------\n");

    if (!strncmp(network_info.device_type, "Ethernet", strlen("Ethernet")))
        ret = show_ethernet_info(interface_name);
    else if (!strncmp(network_info.device_type, "WiFi", strlen("WiFi")))
        ret = show_wifi_info(interface_name);
    else if (!strncmp(network_info.device_type, "Modem", strlen("Modem")))
        ret = show_modem_info(interface_name);
    return ret;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        usage();
        return -1;
    }
    return network_info(argv[1]);
}
