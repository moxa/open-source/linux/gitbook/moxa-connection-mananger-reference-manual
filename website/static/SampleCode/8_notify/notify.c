#include <libmcm/mcm-base-info.h>
#include <libmcm/mcm-listen-event.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int stop_flag = 1;

int get_cb(int *received_prop) {

    printf("received_prop %d \n", *received_prop);
    return 0;
}

void sighandler(int signum) {
    printf("Caught signal %d, coming out...\n", signum);
    stop_flag = 0;
}

void listen_property_changed(char *interface_name) {
    event_context *context = mcm_init_property_changed_event_context();

    int ret = mcm_subscribe_properties_changed_notify_interface(interface_name, context, get_cb);
    if (ret != 0) {
        printf("ERROR: listen interface(%s) property cheanged failed :%d\n", interface_name, ret);
    }

    mcm_start_listen_subscribe_properties_changed_event(context);

    while (stop_flag)
        ;

    mcm_stop_listen_subscribe_properties_changed_event(context);
}

void usage(void) {
    printf("USAGE:\n\rsignal [Interface Name]\n");
}

int main(int argc, char *argv[]) {
    if (argc != 2 || !argv[1])
        usage();

    signal(SIGINT, sighandler);

    listen_property_changed(argv[1]);

    return 0;
}
