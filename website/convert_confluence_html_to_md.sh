#!/bin/bash
# entry MCM directory
rm -rf index.md

find . -name "*.md" -exec sed -i 's/style="border-width: 1px;"//g' {} \;
find . -name "*.md" -exec sed -i '/^## Attachments:$/q' {} \;
find . -name "*.md" -exec sed -i 's/## Attachments://' {} \;
find . -name "*.md" -exec sed -i '/^<div class="pageSectionHeader">$/d' {} \;
find . -name "*.md" -exec sed -i 's/attachments/..\/..\/attachments/g' {} \;
find . -name "*.md" -exec sed -i 's/images/..\/..\/images/g' {} \;
find . -name "*.md" -exec sed -i 's/^#\ \(.*\)$/---\ntitle: \1\n---/' {} \;
find . -name "*.md" -exec  sed -i ':a;N;$!ba;s/<a.*>#<\/a>//g' {} \;
