module.exports = {
  releasenote: [
    'MCM/What_is_Moxa_Connection_Manager',
    'MCM/System_Architecture',
    {
        type: 'category',
        collapsed: true,
        label: 'Configure File',
        items: [
            'MCM/Configuration_file_structure',
            'MCM/MoxaConnectionManager.conf',
            'MCM/Celllular.conf',
            'MCM/WiFi.conf',
            'MCM/P2P_WiFi.conf',
            'MCM/LAN.conf',
        ],
    },
    {
        type: 'category',
        collapsed: true,
        label: 'Getting Started',
        items: [
            'MCM/Basic_Control',
            'MCM/Simple_configuration_for_Cellular_connection',
        ],
    },
    {
        type: 'category',
        collapsed: true,
        label: 'Command Line Tool (CLI)',
        items: [
            'MCM/CLI',
            'MCM/Unlock_SIM_PIN_PUK',
        ],
    },
    'MCM/GUI',
    {
        type: 'category',
        collapsed: true,
        label: 'Setup Cellular Connection',
        items: [
            'MCM/Setup_Cellular_Network_Type',
            'MCM/Configure_GPS_function',
            'MCM/Configure_Cellular_Network_Related_Settings',
            'MCM/Configure_Cellular_Network_Profiles',
        ],
    },
    {
        type: 'category',
        collapsed: true,
        label: 'Setup WiFi Connection',
        items: [
            'MCM/Setup_WiFi_Network_Type',
            'MCM/Configure_WiFi_Network_Related_Settings',
            'MCM/Configure_WiFi_WAN_Network_Profiles',
            'MCM/Configure_WiFi_LAN_Network_Profiles',
        ],
    },
    {
        type: 'category',
        collapsed: true,
        label: 'Setup WiFi-P2P Connection',
        items: [
            'MCM/Setup_WiFi-P2P_Network_Type',
            'MCM/Configure_WiFi-P2P_Network_Profiles',
        ],
    },
    {
        type: 'category',
        collapsed: true,
        label: 'Setup Ethernet Connection',
        items: [
            'MCM/Setup_Ethernet_Network_Type',
            'MCM/Configure_Ethernet_Network_Related_Settings',
            'MCM/Configure_Ethernet_Network_Profiles',
        ],
    },
    {
        type: 'category',
        collapsed: true,
        label: 'Backup interfaces (Failover/Failback)',
        items: [
            'MCM/Configure_Multi_Backup_WAN_interfaces',
            'MCM/Seamless_Failover',
        ],
    },
    'MCM/Network_Interface_Status',
    'MCM/GPS',
    'MCM/Reset_to_default',
    'MCM/Time_Synchronization',
    'MCM/Data_Usage',
    'MCM/Cellular_Disagnosis',
    'MCM/Module_Firmware_Upgrade',
    {
        type: 'category',
        collapsed: true,
        label: 'Other OS (Debian12/Ubuntu 22.04)',
        items: [
            'MCM/Manual_Install_MCM_packages',
            'MCM/Using_networking_service_to_control_Ethernet_instend_of_MCM',
            'MCM/Use_Networkmanager_to_Control_Cellular_and_WiFi_on_Debian_12_Ubuntu_22.04',
        ],
    },

    'MCM/State_Machine',
    {
        type: 'category',
        collapsed: true,
        label: 'Libmcm API Documentation',
        items: [
            'MCM/Libmcm',
            {
              type: 'link',
              label: 'v1.0',
              href: 'https://moxa.gitlab.io/open-source/linux/gitbook/moxa-connection-manager-reference-manual/libmcm-api-docs/VERSION_1_1_0/index.html',
            },
            {
              type: 'link',
              label: 'v1.1',
              href: 'https://moxa.gitlab.io/open-source/linux/gitbook/moxa-connection-manager-reference-manual/libmcm-api-docs/VERSION_1_1_0/index.html',
            },
            {
              type: 'link',
              label: 'v1.2',
              href: 'https://moxa.gitlab.io/open-source/linux/gitbook/moxa-connection-manager-reference-manual/libmcm-api-docs/VERSION_1_1_0/index.html',
            },
            {
              type: 'link',
              label: 'v1.3',
              href: 'https://moxa.gitlab.io/open-source/linux/gitbook/moxa-connection-manager-reference-manual/libmcm-api-docs/VERSION_1_3_0/index.html',
            },
            {
              type: 'link',
              label: 'v1.4',
              href: 'https://moxa.gitlab.io/open-source/linux/gitbook/moxa-connection-manager-reference-manual/libmcm-api-docs/VERSION_1_4_0/index.html',
            },
            {
                type: 'category',
                collapsed: true,
                label: 'Sample Code',
                items: [
                    'MCM/List_All_Interfaces',
                    'MCM/Get_Interface_Information',
                    'MCM/Get_List_of_Interface_Properties',
                    'MCM/Event_Callback',
                    'MCM/Scan_WiFi_AP',
                    'MCM/Interface_Datausage', 
                    'MCM/Control_Moxa-Connection-Manager',
                ],
            },
        ],
    },
    {
        type: 'category',
        collapsed: true,
        label: 'D-Bus Properties',
        items: [
            'MCM/Cellular_D-bus_Properties',
            'MCM/Ethernet_D-bus_Properties',
            'MCM/WiFi_D-bus_Properties',
        ],
    }, 
  ],
};
